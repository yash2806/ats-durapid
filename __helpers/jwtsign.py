from jwt import encode, decode

def create_jwt(email:str,name:str,role:str):
    payload = {
                'email':email,
                'name': name,
                'role':role
            }
    encoded_jwt= encode(payload,'1234afsecret',algorithm='HS256')
    return encoded_jwt

def decode_jwt(token:str):
    decode_token = decode(token,'1234afsecret',algorithms=['HS256'])
    user_info = {
        "role":decode_token['role'],
        "email":decode_token['email'],
        "name":decode_token['name']
    }
    return user_info