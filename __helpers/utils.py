import base64
def encrypt_password(password:str):
    ''' 
    Takes password as input and convert it to encrypted password
    using python bas64 and return it.
    '''
    converted_bytes_password = password.encode("ascii")
    base64_bytes = base64.b64encode(converted_bytes_password)
    base64_password_string = base64_bytes.decode("ascii")
    return base64_password_string

def decrypt_password(encrypted_string:str):
    '''
    Takes encrypted password string and convert it to
    the original string and return it.
    '''
    base64_bytes = encrypted_string.encode("ascii")
    decoded_string_bytes = base64.b64decode(base64_bytes)
    original_string = decoded_string_bytes.decode("ascii")
    return original_string