def success_response(status:int,message:str,token:str):
    success = {
        "status": status,
        "token": token,
        "message" : message,
    }
    return success


def failure_response(status:int,message:str):
    failure = {
        "status":status,
        "message":message,
    }
    return failure

def data_response(status:int,data,message:str,permission=None):
    data =  {
        "status":status,
        "data":data,
        "message":message,
        "permission":permission
    }
    return data