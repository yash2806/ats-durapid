from django.db import models
from adminpanel.models import Company,Designation,Education,GetReference, JobType, Skill, Certification, Language,SoftSkills
from hiring_process.models import BaseModel



class CandidateCentral(BaseModel):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=True, null=True) 
    email = models.CharField(max_length=255, blank=True, null=True)
    mobile_no = models.CharField(max_length=255, blank=True, null=True)
    experience_year = models.CharField(max_length=255, blank=True, null=True)
    experience_months = models.CharField(max_length=255, blank=True, null=True)
    ectc = models.FloatField(blank=True, null=True)  # Field name made lowercase.
    cctc = models.FloatField(blank=True, null=True)  # Field name made lowercase.
    notice_period_days = models.CharField(max_length=255, blank=True, null=True)
    is_screening = models.IntegerField(blank=True, null=True)
    stage = models.IntegerField(blank=True, null=True)
    notice_period_months = models.CharField(max_length=255, blank=True, null=True)
    resume_link = models.CharField(max_length=255, blank=True, null=True)
    isarchive = models.IntegerField(blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    master_candidate_id = models.IntegerField(blank=True, null=True)

class CandidateCompany(BaseModel):
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='company_of_candidate')
    company = models.ForeignKey(Company,on_delete=models.CASCADE, related_name="candidate_company")
    isactive = models.IntegerField(blank=True, null=True)  # Field name made lowercase.

class CandidateDesignation(BaseModel):
    designation = models.ForeignKey(Designation,on_delete=models.CASCADE, related_name="candidate_designation")
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='designation_of_candidate')
    isactive = models.IntegerField(blank=True, null=True)  # Field name made lowercase.
    isapproved = models.IntegerField(blank=True, null=True)  # Field name made lowercase.


class CandidateTechRoundReview(BaseModel):
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='candidate_tech')
    skill_id = models.IntegerField(blank=True, null=True)
    rating = models.IntegerField(blank=True, null=True)
    round = models.CharField(max_length=255, blank=True, null=True)
    mode = models.CharField(max_length=255, blank=True, null=True)

class CandidateEducation(BaseModel):
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='candidate_education')
    education = models.ForeignKey(Education,on_delete=models.CASCADE, related_name="education_of_candidate")
    

class CandidateReference(BaseModel):
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='candidate_reference')
    reference= models.ForeignKey(GetReference,on_delete=models.CASCADE, related_name="reference_of_candidate")
    isactive = models.IntegerField(blank=True, null=True)  # Field name made lowercase.
    isapproved = models.IntegerField(blank=True, null=True)  # Field name made lowercase.
    

class CandidateJobtype(BaseModel):
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='candidate_job_type')
    job_type= models.ForeignKey(JobType,on_delete=models.CASCADE, related_name="job_type_of_candidate")
    isactive = models.IntegerField(blank=True, null=True)  # Field name made lowercase.
    isapproved = models.IntegerField(blank=True, null=True)  # Field name made lowercase.
    

class CandidateSkills(BaseModel):
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='candidate_skills')
    skill= models.ForeignKey(Skill,on_delete=models.CASCADE, related_name="skill_of_candidate")
   

class CandidateStatus(BaseModel):
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='candidate_status')
    is_joined = models.IntegerField(blank=True, null=True)
    

class CandidateCertification(BaseModel):
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='candidate_certification')
    certification= models.ForeignKey(Certification,on_delete=models.CASCADE, related_name="certification_of_candidate")
    isactive = models.IntegerField(blank=True, null=True)  # Field name made lowercase.
    isapproved = models.IntegerField(blank=True, null=True)  # Field name made lowercase.
    

class CandidateLanguage(BaseModel):
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='candidate_language')
    language= models.ForeignKey(Language,on_delete=models.CASCADE, related_name="language_of_candidate")
    

class Feedback(BaseModel):
    stage = models.IntegerField(blank=True, null=True)
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='candidate_feedback')
    comment = models.CharField(max_length=255, blank=True, null=True)

class CandidateInterview(BaseModel):
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='candidate_interview')
    interviewer_id = models.IntegerField(blank=True, null=True)
    invite_time = models.CharField(max_length=255, blank=True, null=True)
    invite_date = models.CharField(max_length=255, blank=True, null=True)
    invite_link = models.CharField(max_length=255, blank=True, null=True)
    reason = models.CharField(max_length=255, blank=True, null=True)
    is_interviewed = models.CharField(max_length=255, blank=True, null=True)
    status = models.CharField(max_length=255, blank=True, null=True)
    comment = models.CharField(max_length=255, blank=True, null=True)
    

class CandidateSoftskills(BaseModel):
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='candidate_soft_skills')
    soft_skill= models.ForeignKey(SoftSkills,on_delete=models.CASCADE, related_name="soft_skill_of_candidate")
    rating = models.CharField(max_length=255, blank=True, null=True)
    

class ClientFeedback(BaseModel):
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='candidate_client_feedback')
    client_name = models.CharField(max_length=255, blank=True, null=True)
    

class HrDiscussion(BaseModel):
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='candidate_hr_discussion')
    candidate_status = models.CharField(max_length=255, blank=True, null=True)
    joining_date = models.DateField(blank=True, null=True)
    mode = models.CharField(max_length=255, blank=True, null=True)
    
class CandidateJoining(BaseModel):
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='candidate_joining')
    offerletter_status = models.CharField(max_length=255, blank=True, null=True)
    joined_date = models.DateField(blank=True, null=True)
    
class CandidateTechInterviewLogs(BaseModel):
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='candidate_tech_interview_logs')
    interviewer_id = models.CharField(max_length=255, blank=True, null=True)
    invite_time = models.CharField(max_length=255, blank=True, null=True)
    invite_date = models.CharField(max_length=255, blank=True, null=True)
    invite_link = models.CharField(max_length=255, blank=True, null=True)
    reason = models.CharField(max_length=255, blank=True, null=True)
    round = models.IntegerField(blank=True, null=True)
    is_interviewed = models.CharField(max_length=255, blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)
    stage = models.IntegerField(blank=True, null=True)

class CandidateTechInterview(BaseModel):
    candidate = models.ForeignKey(CandidateCentral, on_delete=models.CASCADE, related_name='candidate_tech_interview')
    interviewer_id = models.CharField(max_length=255, blank=True, null=True)
    invite_time = models.CharField(max_length=255, blank=True, null=True)
    invite_date = models.CharField(max_length=255, blank=True, null=True)
    invite_link = models.CharField(max_length=255, blank=True, null=True)
    reason = models.CharField(max_length=255, blank=True, null=True)
    round = models.IntegerField(blank=True, null=True)
    is_interviewed = models.CharField(max_length=255, blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)


    
