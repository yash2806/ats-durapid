from rest_framework.views import APIView
from rest_framework import status
# from login.models import Userlogin
from .helper_func import verified_token, current_date, payload_mapper, current_date_time
from .models import *
from .serializers import *
from django.conf import settings
from django.db import connections
import os
import requests
from adminpanel.models import *
import pandas as pd
from adminpanel.serializers import *
from datetime import datetime,date
from rest_framework.response import Response
from django.http import JsonResponse
# from resume_parser import resumeparse
# from pyresparser import ResumeParser
from __helpers.jwtsign import create_jwt, decode_jwt
from __helpers.responce import success_response, failure_response, data_response
from candidate.mapping import *
from django.db.models import Q
from rest_framework.viewsets import ModelViewSet
from .permissions import IsAdmin, IsHR, IsMember
from rest_framework.permissions import IsAuthenticated


# df = pd.read_csv(os.path.join(os.getcwd(), 'combined_data.csv'))
# combined_skills_data = [x.lower() for x in df['skills']]


# class ResumeParseAPI(APIView):
#     def __init__(self) -> None:
#         pass

#     def post(self, req) -> Response:
#         try:
#             token = verified_token(req)
#             email = token['email']
#             role = token['role']
#             if role == 'User':
#                 return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are Not Authorized"))
#             else:
#                 hr_obj = Userlogin.objects.filter(email=email)
#                 file = req.FILES['file']
#                 file_name = str(file).split(".")[0].replace(' ', '_')
#                 file_extension = str(file).split('.')[1]
#                 full_file_name = f'{file_name}.{file_extension}'
#                 with open(os.path.join(settings.MEDIA_ROOT, full_file_name), 'wb+') as destination:
#                     for chunk in file.chunks():
#                         destination.write(chunk)
#                 data_resume_parser = resumeparse.read_file(os.path.join(settings.MEDIA_ROOT, full_file_name))
#                 data_pyresparser = ResumeParser(os.path.join(settings.MEDIA_ROOT, full_file_name)).get_extracted_data()
#                 main_data = {}
#                 main_data['name'] = ' '.join(list(set([
#                     f"{data_pyresparser['name'].lower() if data_pyresparser['name'] else '' }",
#                     f"{data_resume_parser['name'].lower() if data_resume_parser['name'] else '' }"
#                 ])))

#                 main_data['email'] = ''.join(list(set([
#                     f"{data_pyresparser['email'] if data_pyresparser['email'] else '' }",
#                     f"{data_resume_parser['email'] if data_resume_parser['email'] else '' }"
#                 ])))

#                 main_data['mobile'] = list(set([
#                     f"{data_pyresparser['mobile_number'] if data_pyresparser['mobile_number'] else '' }",
#                     f"{data_resume_parser['phone'] if data_resume_parser['phone'] else '' }"
#                 ]))

#                 main_data['skills'] = [skill.lower() for skill in list(
#                     set(list(data_pyresparser['skills']) + list(data_resume_parser['skills'])))]
#                 main_data['degree'] = sum([
#                     data_pyresparser['degree'] if data_pyresparser['degree'] else [
#                     ],
#                     data_resume_parser['degree'] if data_resume_parser['degree'] else [
#                     ]
#                 ], [])

#                 main_data['company_names'] = data_pyresparser['company_names'] if data_pyresparser['company_names'] else []

#                 main_data['university'] = sum([
#                     data_pyresparser['college_name'] if data_pyresparser['college_name'] else [
#                     ],
#                     list(set(data_resume_parser['university'])
#                          ) if data_resume_parser['university'] else []
#                 ], [])
#                 main_data['resume_link'] = f'/media/{full_file_name}'
#         except Exception as e:
#             return JsonResponse(failure_response(status.HTTP_401_UNAUTHORIZED, f'{str(e)}'))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, data=main_data, message="Fetched Details"))

# class PostCandidateForm(APIView):
#     def post(self, request, *args, **kwargs):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             try:
#                 user_obj = Userlogin.objects.get(email=email)
#             except Exception as e:
#                 user_obj = TeamMembers.objects.get(email=email)
#             user_id = user_obj.id
#             if user_obj:
#                 if user_obj.isactive:
#                     data = request.data
#                     if role == 'User':
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are Not Authorized"))
                    
#                     if role == "Admin" or role == "HR":
#                         candidate_email = data['email']
#                         try:
#                             cand_obj = CandidateCentral.objects.filter(Q(email=candidate_email) & Q(isarchive=False))
#                         except Exception as e:
#                             cand_obj = None
#                         if len(cand_obj) > 0:
#                             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="User Already Exists"))
#                         form_data = payload_mapper(
#                             payload=data, modelField=candidate_form)
#                         form_data['created_by'] = user_id
#                         form_data['created_date'] = current_date_time()
#                         form_data['modified_by'] = user_id
#                         form_data['modified_date'] = current_date_time()
#                         form_data['isarchive'] = False
#                         form = CandidateCentral.objects.create(**form_data)
#                         try:
#                             candidate_id = CandidateCentral.objects.get(
#                                 Q(email=candidate_email) & Q(isarchive=False)).id
#                         except Exception as e:
#                             candidate_id = None
#                         if candidate_id:
#                             if 'skills' in data.keys():
#                                 for i in data['skills']:
#                                     CandidateSkills.objects.create(
#                                         candidate_id=candidate_id, skill_id=i, created_by=user_id, modified_by=user_id)

#                             if 'certification' in data.keys():
#                                 for i in data['certification']:
#                                     CandidateCertification.objects.create(
#                                         candidate_id=candidate_id, certification_id=i, created_by=user_id, modified_by=user_id)

#                             if 'language' in data.keys():
#                                 for i in data['language']:
#                                     CandidateLanguage.objects.create(
#                                         candidate_id=candidate_id, language_id=i, created_by=user_id, modified_by=user_id)

#                             if 'current_company' in data.keys():
#                                 CandidateCompany.objects.create(
#                                     candidate_id=candidate_id, company_id=data['current_company'], created_by=user_id, modified_by=user_id)

#                             if 'designation' in data.keys():
#                                 CandidateDesignation.objects.create(
#                                     candidate_id=candidate_id, designation_id=data['designation'], created_by=user_id, modified_by=user_id)

#                             if 'education' in data.keys():
#                                 for i in data['education']:
#                                     CandidateEducation.objects.create(
#                                         candidate_id=candidate_id, education_id=i, created_by=user_id, modified_by=user_id)

#                             if 'prefered_job_type' in data.keys():
#                                 CandidateJobtype.objects.create(
#                                     candidate_id=candidate_id, jobtype_id=data['prefered_job_type'], created_by=user_id, modified_by=user_id)

#                             if 'reference_mode' in data.keys():
#                                 for i in data['reference_mode']:
#                                     CandidateReference.objects.create(
#                                         candidate_id=form.id, reference_id=i, created_by=user_id, modified_by=user_id)
                    
#                     elif role == "Member":
#                         permission_obj = Permission.objects.get(member_id=user_id)
#                         if permission_obj.can_create == False :
#                            return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You don't have permission to add candidate"))
#                         candidate_email = data['email']
#                         try:
#                             cand_obj = CandidateCentral.objects.filter(Q(email=candidate_email) & Q(isarchive=False))
#                         except Exception as e:
#                             cand_obj = None
#                         if len(cand_obj) > 0:
#                             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="User Already Exists"))
#                         form_data = payload_mapper(
#                             payload=data, modelField=candidate_form)
#                         form_data['created_by'] = user_id
#                         form_data['created_date'] = current_date_time()
#                         form_data['modified_by'] = user_id
#                         form_data['modified_date'] = current_date_time()
#                         form_data['isarchive'] = False
#                         form = CandidateCentral.objects.create(**form_data)
#                         try:
#                             candidate_id = CandidateCentral.objects.get(Q(email=candidate_email) & Q(isarchive=False)).id
#                         except Exception as e:
#                             candidate_id = None
#                         if candidate_id:
#                             if 'skills' in data.keys():
#                                 for i in data['skills']:
#                                     CandidateSkills.objects.create(
#                                         candidate_id=candidate_id, skill_id=i, created_by=user_id, modified_by=user_id)

#                             if 'certification' in data.keys():
#                                 for i in data['certification']:
#                                     CandidateCertification.objects.create(
#                                         candidate_id=candidate_id, certification_id=i, created_by=user_id, modified_by=user_id)

#                             if 'language' in data.keys():
#                                 for i in data['language']:
#                                     CandidateLanguage.objects.create(
#                                         candidate_id=candidate_id, language_id=i, created_by=user_id, modified_by=user_id)

#                             if 'current_company' in data.keys():
#                                 CandidateCompany.objects.create(
#                                     candidate_id=candidate_id, company_id=data['current_company'], created_by=user_id, modified_by=user_id)

#                             if 'designation' in data.keys():
#                                 CandidateDesignation.objects.create(
#                                     candidate_id=candidate_id, designation_id=data['designation'], created_by=user_id, modified_by=user_id)

#                             if 'education' in data.keys():
#                                 for i in data['education']:
#                                     CandidateEducation.objects.create(
#                                         candidate_id=candidate_id, education_id=i, created_by=user_id, modified_by=user_id)

#                             if 'prefered_job_type' in data.keys():
#                                 CandidateJobtype.objects.create(
#                                     candidate_id=candidate_id, jobtype_id=data['prefered_job_type'], created_by=user_id, modified_by=user_id)

#                             if 'reference_mode' in data.keys():
#                                 for i in data['reference_mode']:
#                                     CandidateReference.objects.create(candidate_id=form.id, reference_id=i, created_by=user_id, modified_by=user_id)
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_201_CREATED, message="Form added successfully", data=None))


# class ListCandidates(APIView):
#     def get(self, request, *args, **kwargs):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             try:
#                 user_obj = Userlogin.objects.get(email=email)
#             except Exception as e:
#                 user_obj = TeamMembers.objects.get(email=email)
#             user_id = user_obj.id
#             if user_obj:
#                 if user_obj.isactive:
#                     today = request.query_params.get('today')
#                     start_date = request.query_params.get('start_date')
#                     end_date = request.query_params.get('end_date')
#                     if role == 'HR' or role == "Member":
#                         if (start_date != None and (end_date == None and today==None)):
#                             from_date = datetime.strptime(start_date,'%Y-%m-%d').date()
#                             candidate_obj = CandidateCentral.objects.filter(Q(created_by=user_id) & Q(created_date__gte=from_date) & Q(isarchive=False))
                        
#                         elif(end_date != None and (start_date == None and today==None)):
#                             to_date = datetime.strptime(end_date,'%Y-%m-%d').date()
#                             candidate_obj = CandidateCentral.objects.filter(Q(created_by=user_id) & Q(created_date__lte=to_date)& Q(isarchive=False))
                        
#                         elif (start_date == None and end_date == None and today == None):
#                             candidate_obj = CandidateCentral.objects.filter(Q(created_by=user_id)& Q(isarchive=False))
                        
#                         elif ((start_date == None and end_date == None) and today != None): 
#                             today_date = datetime.strptime(today,'%Y-%m-%d').date()
#                             candidate_obj = CandidateCentral.objects.filter(Q(created_by=user_id) & Q(created_date=today_date)& Q(isarchive=False))
                        
#                         elif((start_date!=None and end_date!=None) and today == None):
#                             from_date = datetime.strptime(start_date,'%Y-%m-%d').date()
#                             to_date = datetime.strptime(end_date,'%Y-%m-%d').date()
#                             candidate_obj = CandidateCentral.objects.filter(Q(created_by=user_id) & Q(created_date__range=[from_date,to_date])& Q(isarchive=False))
                        
#                         candidate_serialized_list = CandidateCentralSerializer(candidate_obj, many=True).data
#                         output = []
#                         for candidate in candidate_serialized_list:
#                             candidate = dict(candidate)
#                             desg_id = CandidateDesignation.objects.get(candidate_id=candidate['id']).designation_id
#                             designation = Designation.objects.get(id=desg_id).name
#                             selected_by_name = Userlogin.objects.get(id=candidate['created_by']).name
#                             try:
#                                 skilll_id = CandidateSkills.objects.filter(candidate_id=candidate['id'])
#                             except Exception as e:
#                                 skilll_id = None
#                             if skilll_id:
#                                 skills_arr = []
#                                 for skill in skilll_id:
#                                     skill = Skill.objects.get(
#                                         id=skill.skill_id).name
#                                     skills_arr.append(skill)
#                                 candidate['skills'] = skills_arr
#                                 candidate['designation'] = designation
#                                 candidate['selected'] = selected_by_name
#                                 output.append(candidate)
                    
#                     elif role == 'Admin':
#                         if (start_date != None and (end_date == None and today==None)):
#                             from_date = datetime.strptime(start_date,'%Y-%m-%d').date()
#                             candidate_obj = CandidateCentral.objects.filter(Q(created_date__gte=from_date))
                        
#                         elif(end_date != None and (start_date == None and today==None)):
#                             print("inside elif")
#                             to_date = datetime.strptime(end_date,'%Y-%m-%d').date()
#                             candidate_obj = CandidateCentral.objects.filter(Q(created_date__lte=to_date))
                        
#                         elif (start_date == None and end_date == None and today == None):
#                             print("inside 297")
#                             candidate_obj = CandidateCentral.objects.all()
#                             print(candidate_obj)
                        
#                         elif ((start_date == None and end_date == None) and today != None): 
#                             today_date = datetime.strptime(today,'%Y-%m-%d').date()
#                             candidate_obj = CandidateCentral.objects.filter(Q(created_date=today_date))
                        
#                         elif((start_date!=None and end_date!=None) and today == None):
#                             from_date = datetime.strptime(start_date,'%Y-%m-%d').date()
#                             to_date = datetime.strptime(end_date,'%Y-%m-%d').date()
#                             candidate_obj = CandidateCentral.objects.filter(Q(created_date__range=[from_date,to_date]))
#                         candidate_serialized_list = CandidateCentralSerializer(candidate_obj, many=True).data
#                         print("310",candidate_serialized_list)
#                         output = []
                        
#                         for candidate in candidate_serialized_list:
#                             candidate = dict(candidate)
#                             print("315",candidate)
#                             desg_id = CandidateDesignation.objects.get(candidate_id=candidate['id']).designation_id
#                             print("317",desg_id)
#                             designation = Designation.objects.get(id=desg_id).name
#                             print("319",designation)
#                             selected_by_name = Userlogin.objects.get(id=candidate['created_by']).name
#                             print("321",selected_by_name)
#                             try:
#                                 skilll_id = CandidateSkills.objects.filter(candidate_id=candidate['id'])
#                                 print("324",skilll_id)
#                             except Exception as e:
#                                 skilll_id = None
#                             if skilll_id:
#                                 skills_arr = []
#                                 for skill in skilll_id:
#                                     skill = Skill.objects.get(id=skill.skill_id).name
#                                     print("331",skill)
#                                     skills_arr.append(skill)
#                                     print("333",skills_arr)
#                                 candidate['skills'] = skills_arr
#                             candidate['designation'] = designation
#                             candidate['selected'] = selected_by_name
#                             print("337",candidate)
#                             output.append(candidate)
                    
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, data=output, message="Data fetched successfully"))


# class EditCandidate(APIView):
#     def post(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             try:
#                 user_obj = Userlogin.objects.get(email=email)
#             except Exception as e:
#                 user_obj = TeamMembers.objects.get(email=email)
#             user_id = user_obj.id
#             if user_obj:
#                 if user_obj.isactive:
#                     data = request.data
#                     if role == 'User':
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are Not Authorized"))
#                     elif role == "Admin" or role == "HR": 
#                         candidate_email = data['email']
#                         if len(CandidateCentral.objects.filter(Q(email=candidate_email) & Q(isarchive=False))) < 0:
#                             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Something went wrong"))
#                         form_data = payload_mapper(payload=data, modelField=candidate_form)
#                         form_data['created_by'] = user_id
#                         form_data['created_date'] = current_date_time()
#                         form_data['modified_by'] = user_id
#                         form_data['modified_date'] = current_date_time()
#                         form = CandidateCentral.objects.filter(Q(email=candidate_email) & Q(isarchive=False)).update(**form_data)
#                         candidate_id = CandidateCentral.objects.get(Q(email=candidate_email) & Q(isarchive=False)).id

#                         if 'skills' in data.keys():
#                             CandidateSkills.objects.filter(candidate_id=candidate_id).delete()
#                             for i in data['skills']:
#                                 CandidateSkills.objects.create(candidate_id=candidate_id, skill_id=i, created_by=user_id, modified_by=user_id)

#                         if 'certification' in data.keys():
#                             CandidateCertification.objects.filter(candidate_id=candidate_id).delete()
#                             for i in data['certification']:
#                                 CandidateCertification.objects.create(candidate_id=candidate_id, certification_id=i, created_by=user_id, modified_by=user_id)

#                         if 'language' in data.keys():
#                             CandidateLanguage.objects.filter(candidate_id=candidate_id).delete()
#                             for i in data['language']:
#                                 CandidateLanguage.objects.create(candidate_id=candidate_id, language_id=i, created_by=user_id, modified_by=user_id)

#                         if 'current_company' in data.keys():
#                             CandidateCompany.objects.filter(candidate_id=candidate_id).delete()
#                             CandidateCompany.objects.create(candidate_id=candidate_id, company_id=data['current_company'], created_by=user_id, modified_by=user_id)

#                         if 'designation' in data.keys():
#                             CandidateDesignation.objects.filter(candidate_id=candidate_id).delete()
#                             CandidateDesignation.objects.create(candidate_id=candidate_id, designation_id=data['designation'], created_by=user_id, modified_by=user_id)

#                         if 'education' in data.keys():
#                             CandidateEducation.objects.filter(candidate_id=candidate_id).delete()
#                             for i in data['education']:
#                                 CandidateEducation.objects.create(candidate_id=candidate_id, education_id=i, created_by=user_id, modified_by=user_id)

#                         if 'prefered_job_type' in data.keys():
#                             CandidateJobtype.objects.filter(candidate_id=candidate_id).delete()
#                             CandidateJobtype.objects.create(candidate_id=candidate_id, jobtype_id=data['prefered_job_type'], created_by=user_id, modified_by=user_id)

#                         if 'reference_mode' in data.keys():
#                             CandidateReference.objects.filter(candidate_id=candidate_id).delete()
#                             for i in data['reference_mode']:
#                                 CandidateReference.objects.create(candidate_id=candidate_id, reference_id=i, created_by=user_id, modified_by=user_id)
                    
#                     elif role == "Member":
#                         permission_obj = Permission.objects.get(member_id=user_id)
#                         if permission_obj.can_update == False :
#                            return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You don't have permission to edit candidate"))
#                         candidate_email = data['email']
#                         if len(CandidateCentral.objects.filter(Q(email=candidate_email) & Q(isarchive=False))) < 0:
#                             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Something went wrong"))
#                         form_data = payload_mapper(payload=data, modelField=candidate_form)
#                         form_data['created_by'] = user_id
#                         form_data['created_date'] = current_date_time()
#                         form_data['modified_by'] = user_id
#                         form_data['modified_date'] = current_date_time()
#                         form = CandidateCentral.objects.filter(Q(email=candidate_email) & Q(isarchive=False)).update(**form_data)
#                         candidate_id = CandidateCentral.objects.get(Q(email=candidate_email) & Q(isarchive=False)).id

#                         if 'skills' in data.keys():
#                             CandidateSkills.objects.filter(candidate_id=candidate_id).delete()
#                             for i in data['skills']:
#                                 CandidateSkills.objects.create(candidate_id=candidate_id, skill_id=i, created_by=user_id, modified_by=user_id)

#                         if 'certification' in data.keys():
#                             CandidateCertification.objects.filter(candidate_id=candidate_id).delete()
#                             for i in data['certification']:
#                                 CandidateCertification.objects.create(candidate_id=candidate_id, certification_id=i, created_by=user_id, modified_by=user_id)

#                         if 'language' in data.keys():
#                             CandidateLanguage.objects.filter(candidate_id=candidate_id).delete()
#                             for i in data['language']:
#                                 CandidateLanguage.objects.create(candidate_id=candidate_id, language_id=i, created_by=user_id, modified_by=user_id)

#                         if 'current_company' in data.keys():
#                             CandidateCompany.objects.filter(candidate_id=candidate_id).delete()
#                             CandidateCompany.objects.create(candidate_id=candidate_id, company_id=data['current_company'], created_by=user_id, modified_by=user_id)

#                         if 'designation' in data.keys():
#                             CandidateDesignation.objects.filter(candidate_id=candidate_id).delete()
#                             CandidateDesignation.objects.create(candidate_id=candidate_id, designation_id=data['designation'], created_by=user_id, modified_by=user_id)

#                         if 'education' in data.keys():
#                             CandidateEducation.objects.filter(candidate_id=candidate_id).delete()
#                             for i in data['education']:
#                                 CandidateEducation.objects.create(candidate_id=candidate_id, education_id=i, created_by=user_id, modified_by=user_id)

#                         if 'prefered_job_type' in data.keys():
#                             CandidateJobtype.objects.filter(candidate_id=candidate_id).delete()
#                             CandidateJobtype.objects.create(candidate_id=candidate_id, jobtype_id=data['prefered_job_type'], created_by=user_id, modified_by=user_id)

#                         if 'reference_mode' in data.keys():
#                             CandidateReference.objects.filter(candidate_id=candidate_id).delete()
#                             for i in data['reference_mode']:
#                                 CandidateReference.objects.create(candidate_id=candidate_id, reference_id=i, created_by=user_id, modified_by=user_id)
                        
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_201_CREATED, message="Form Updated successfully", data=None))


# class GetCandidateInfo(APIView):
#     def get(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             try:
#                 user_obj = Userlogin.objects.get(email=email)
#             except Exception as e:
#                 user_obj = TeamMembers
#             user_id = user_obj.id
#             if user_obj:
#                 if user_obj.isactive:
#                     data = request.data
#                     if role == 'User' or role == 'Admin':
#                         candi_id = request.query_params.get('id')
#                         candidate_obj = CandidateCentral.objects.get(Q(id=candi_id) & Q(isarchive=False))
#                         candidate_serialized_obj = CandidateCentralSerializer(candidate_obj).data
#                         designation_id = CandidateDesignation.objects.get(candidate_id=candi_id).designation_id
#                         designation_name = DesignationSerializer(Designation.objects.get(id=designation_id)).data['name']
#                         company_id = CandidateCompany.objects.get(candidate_id=candi_id).company_id
#                         company_name = CompanySerializer(Company.objects.get(id=company_id)).data['name']
#                         jobtype_id = CandidateJobtype.objects.get(candidate_id=candi_id).jobtype_id
#                         jobtype_name = JobTypeSerializer(JobType.objects.get(id=jobtype_id)).data['name']
#                         skilll_id = CandidateSkills.objects.filter(candidate_id=candi_id)
#                         skills_arr = []
#                         for skill in skilll_id:
#                             skill_name = SkillsSerializer(Skill.objects.get(id=skill.skill_id)).data
#                             c = {"id": skill_name['id'],
#                                  "name": skill_name['name']}
#                             skills_arr.append(c)
#                         certificate_id = CandidateCertification.objects.filter(candidate_id=candi_id)
#                         certificate_array = []
#                         for certificate in certificate_id:
#                             certificate = CertificationSerializer(Certification.objects.get(id=certificate.certification_id)).data
#                             c = {"id": certificate['id'],
#                                  "name": certificate['name']}
#                             certificate_array.append(c)
#                         edu_id = CandidateEducation.objects.filter(candidate_id=candi_id)
#                         education_array = []
#                         for education in edu_id:
#                             education = EducationSerializer(Education.objects.get(id=education.education_id)).data
#                             c = {"id": education['id'],
#                                  "name": education['name']}
#                             education_array.append(c)
#                         language_id = CandidateLanguage.objects.filter(candidate_id=candi_id)
#                         language_array = []
#                         for language in language_id:
#                             language = LanguageSerializer(Language.objects.get(id=language.language_id)).data
#                             c = {"id": language['id'],
#                                  "name": language['name']}
#                             language_array.append(c)
#                         referencemode_id = CandidateReference.objects.filter(candidate_id=candi_id)
#                         reference_array = []
#                         for reference in referencemode_id:
#                             reference = GetReferenceSerializer(GetReference.objects.get(id=reference.reference_id)).data
#                             c = {"id": reference['id'],
#                                  "name": reference['name']}
#                             reference_array.append(c)
#                         finalOP = {
#                             "Candidate": candidate_serialized_obj,
#                             "company_name": company_name,
#                             "designation": designation_name,
#                             "prefered_job_type": jobtype_name,
#                             "reference_mode": reference_array,
#                             "skills": skills_arr,
#                             "certificate_id": certificate_array,
#                             "education": education_array,
#                             "language": language_array
#                         }
                    
#                     elif role == 'HR':
#                         candi_id = request.query_params.get('id')
#                         candidate_obj = CandidateCentral.objects.get(Q(id=candi_id) & Q(isarchive=False) & Q(created_by=user_id))
#                         candidate_serialized_obj = CandidateCentralSerializer(candidate_obj).data
#                         designation_id = CandidateDesignation.objects.get(candidate_id=candi_id).designation_id
#                         designation_name = DesignationSerializer(Designation.objects.get(id=designation_id)).data['name']
#                         company_id = CandidateCompany.objects.get(candidate_id=candi_id).company_id
#                         company_name = CompanySerializer(Company.objects.get(id=company_id)).data['name']
#                         jobtype_id = CandidateJobtype.objects.get(candidate_id=candi_id).jobtype_id
#                         jobtype_name = JobTypeSerializer(JobType.objects.get(id=jobtype_id)).data['name']
#                         skilll_id = CandidateSkills.objects.filter(candidate_id=candi_id)
#                         skills_arr = []
#                         for skill in skilll_id:
#                             skill_name = SkillsSerializer(Skill.objects.get(id=skill.skill_id)).data
#                             c = {"id": skill_name['id'],
#                                  "name": skill_name['name']}
#                             skills_arr.append(c)
#                         certificate_id = CandidateCertification.objects.filter(candidate_id=candi_id)
#                         certificate_array = []
#                         for certificate in certificate_id:
#                             certificate = CertificationSerializer(Certification.objects.get(id=certificate.certification_id)).data
#                             c = {"id": certificate['id'],
#                                  "name": certificate['name']}
#                             certificate_array.append(c)
#                         edu_id = CandidateEducation.objects.filter(candidate_id=candi_id)
#                         education_array = []
#                         for education in edu_id:
#                             education = EducationSerializer(Education.objects.get(id=education.education_id)).data
#                             c = {"id": education['id'],
#                                  "name": education['name']}
#                             education_array.append(c)
#                         language_id = CandidateLanguage.objects.filter(candidate_id=candi_id)
#                         language_array = []
#                         for language in language_id:
#                             language = LanguageSerializer(
#                                 Language.objects.get(id=language.language_id)).data
#                             c = {"id": language['id'],
#                                  "name": language['name']}
#                             language_array.append(c)
#                         referencemode_id = CandidateReference.objects.filter(
#                             candidate_id=candi_id)
#                         reference_array = []
#                         for reference in referencemode_id:
#                             reference = GetReferenceSerializer(GetReference.objects.get(id=reference.reference_id)).data
#                             c = {"id": reference['id'],
#                                  "name": reference['name']}
#                             reference_array.append(c)
#                         finalOP = {
#                             "Candidate": candidate_serialized_obj,
#                             "company_name": company_name,
#                             "designation": designation_name,
#                             "prefered_job_type": jobtype_name,
#                             "reference_mode": reference_array,
#                             "skills": skills_arr,
#                             "certificate_id": certificate_array,
#                             "education": education_array,
#                             "language": language_array
#                         }
        
#                     elif role == "Member":
#                         permission_obj = Permission.objects.get(member_id=user_id)
#                         if permission_obj.can_create == False :
#                            return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You don't have permission to list candidate"))
#                         candi_id = request.query_params.get('id')
#                         candidate_obj = CandidateCentral.objects.get(Q(id=candi_id) & Q(isarchive=False) & Q(created_by=user_id))
#                         candidate_serialized_obj = CandidateCentralSerializer(candidate_obj).data
#                         designation_id = CandidateDesignation.objects.get(candidate_id=candi_id).designation_id
#                         designation_name = DesignationSerializer(Designation.objects.get(id=designation_id)).data['name']
#                         company_id = CandidateCompany.objects.get(candidate_id=candi_id).company_id
#                         company_name = CompanySerializer(Company.objects.get(id=company_id)).data['name']
#                         jobtype_id = CandidateJobtype.objects.get(candidate_id=candi_id).jobtype_id
#                         jobtype_name = JobTypeSerializer(JobType.objects.get(id=jobtype_id)).data['name']
#                         skilll_id = CandidateSkills.objects.filter(candidate_id=candi_id)
#                         skills_arr = []
#                         for skill in skilll_id:
#                             skill_name = SkillsSerializer(Skill.objects.get(id=skill.skill_id)).data
#                             c = {"id": skill_name['id'],
#                                  "name": skill_name['name']}
#                             skills_arr.append(c)
#                         certificate_id = CandidateCertification.objects.filter(candidate_id=candi_id)
#                         certificate_array = []
#                         for certificate in certificate_id:
#                             certificate = CertificationSerializer(Certification.objects.get(id=certificate.certification_id)).data
#                             c = {"id": certificate['id'],
#                                  "name": certificate['name']}
#                             certificate_array.append(c)
#                         edu_id = CandidateEducation.objects.filter(candidate_id=candi_id)
#                         education_array = []
#                         for education in edu_id:
#                             education = EducationSerializer(Education.objects.get(id=education.education_id)).data
#                             c = {"id": education['id'],
#                                  "name": education['name']}
#                             education_array.append(c)
#                         language_id = CandidateLanguage.objects.filter(candidate_id=candi_id)
#                         language_array = []
#                         for language in language_id:
#                             language = LanguageSerializer(
#                                 Language.objects.get(id=language.language_id)).data
#                             c = {"id": language['id'],
#                                  "name": language['name']}
#                             language_array.append(c)
#                         referencemode_id = CandidateReference.objects.filter(
#                             candidate_id=candi_id)
#                         reference_array = []
#                         for reference in referencemode_id:
#                             reference = GetReferenceSerializer(GetReference.objects.get(id=reference.reference_id)).data
#                             c = {"id": reference['id'],
#                                  "name": reference['name']}
#                             reference_array.append(c)
#                         finalOP = {
#                             "Candidate": candidate_serialized_obj,
#                             "company_name": company_name,
#                             "designation": designation_name,
#                             "prefered_job_type": jobtype_name,
#                             "reference_mode": reference_array,
#                             "skills": skills_arr,
#                             "certificate_id": certificate_array,
#                             "education": education_array,
#                             "language": language_array
#                         }
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, data=finalOP, message="Data fetched successfully"))


# class DeleteUserObj(APIView):
#     def delete(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             try:
#                 user_obj = Userlogin.objects.get(email=email)
#             except Exception as e:
#                 user_obj = TeamMembers.objects.get(email=email)
#             user_id = user_obj.id
#             if user_obj:
#                 if user_obj.isactive:
#                     id = request.query_params.get('id')
#                     if role == 'User':
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are Not Authorized"))
                    
#                     elif role == "Admin" or role == "HR":
#                         try:
#                             candidate_obj = CandidateCentral.objects.get(Q(id=id)).id
#                         except Exception as e:
#                             candidate_obj = None
#                         if candidate_obj:
#                             CandidateCentral.objects.filter(Q(id=candidate_obj)).update(isarchive=True)
                
#                     elif role == "Member":
#                         permission_obj = Permission.objects.get(member_id=user_id)
#                         if permission_obj.can_delete == False :
#                            return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You don't have permission to delete candidate"))
#                         try:
#                             candidate_obj = CandidateCentral.objects.get(Q(id=id)).id
#                         except Exception as e:
#                             candidate_obj = None
#                         if candidate_obj:
#                             CandidateCentral.objects.filter(Q(id=candidate_obj)).update(isarchive=True)
#                 else:
#                     return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, data=None, message="User Deleted successfully"))


# class GetCandidateInfoInterview(APIView):
#     def get(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             try:
#                 user_obj = Userlogin.objects.get(email=email)
#             except Exception as e:
#                 user_obj = TeamMembers.objects.get(email=email)
#             user_id = user_obj.id
#             if user_obj:
#                 if user_obj.isactive:
#                     today = request.query_params.get('today')
#                     start_date = request.query_params.get('start_date')
#                     end_date = request.query_params.get('end_date')
#                     if role == 'Admin' or role == 'User':
#                         output = []
#                         if (start_date != None and (end_date == None and today==None)):
#                             from_date = datetime.strptime(start_date,'%Y-%m-%d').date()
#                             candidates_obj = CandidateCentralSerializer(CandidateCentral.objects.filter(Q(is_screening__gt=0) & Q(isarchive=False) & Q(created_date__gte=from_date)),many=True).data
                        
#                         elif(end_date != None and (start_date == None and today==None)):
#                             to_date = datetime.strptime(end_date,'%Y-%m-%d').date()
#                             candidates_obj = CandidateCentralSerializer(CandidateCentral.objects.filter(Q(is_screening__gt=0) & Q(isarchive=False) & Q(created_date__lte=to_date)),many=True).data
#                             print("length",len(candidates_obj))
                        
#                         elif (start_date == None and end_date == None and today == None):
#                             candidates_obj = CandidateCentralSerializer(CandidateCentral.objects.filter(Q(is_screening__gt=0) & Q(isarchive=False)),many=True).data
                        
#                         elif ((start_date == None and end_date == None) and today != None):
#                             today_date = datetime.strptime(today,'%Y-%m-%d').date()
#                             candidates_obj = CandidateCentralSerializer(CandidateCentral.objects.filter(Q(is_screening__gt=0) & Q(isarchive=False) & Q(created_date=today_date)),many=True).data

#                         elif((start_date !=None and end_date!=None) and today== None):
#                             from_date = datetime.strptime(start_date,'%Y-%m-%d').date()
#                             to_date = datetime.strptime(end_date,'%Y-%m-%d').date()
#                             candidates_obj = CandidateCentralSerializer(CandidateCentral.objects.filter(Q(is_screening__gt=0) & Q(isarchive=False) & Q(created_date__range = [from_date,to_date])),many=True).data
                    
#                         for candidate in candidates_obj:
#                             try:
#                                 designations = CandidateDesignation.objects.filter(candidate_id=candidate['id'])
#                                 for des in designations:
#                                     designation_name = Designation.objects.get(id=des.designation_id).name
#                                     candidate['designation'] = designation_name
#                             except:
#                                 candidate['designation'] = ""
#                             try:
#                                 skills = CandidateSkills.objects.filter(candidate_id=candidate['id'])
#                                 skill = []
#                                 for sk in skills:
#                                     skills_name = Skill.objects.get(id=sk.skill_id).name
#                                     skill.append(skills_name)
#                                 candidate['skills'] = skill
#                             except:
#                                 candidate['skills'] = []
#                             output.append(candidate)    

#                     elif role == 'HR':
#                         output = []
#                         if (start_date != None and (end_date == None and today==None)):
#                             from_date = datetime.strptime(start_date,'%Y-%m-%d').date()
#                             candidates_obj = CandidateCentralSerializer(CandidateCentral.objects.filter(Q(is_screening__gt=0) & Q(isarchive=False) & Q(created_date__gte=from_date) & Q(created_by=user_id)),many=True).data
                        
#                         elif(end_date != None and (start_date == None and today==None)):
#                             to_date = datetime.strptime(end_date,'%Y-%m-%d').date()
#                             candidates_obj = CandidateCentralSerializer(CandidateCentral.objects.filter(Q(is_screening__gt=0) & Q(isarchive=False) & Q(created_date__lte=to_date) & Q(created_by=user_id)),many=True).data
#                             print("length",len(candidates_obj))
                        
#                         elif (start_date == None and end_date == None and today == None):
#                             candidates_obj = CandidateCentralSerializer(CandidateCentral.objects.filter(Q(is_screening__gt=0) & Q(isarchive=False) & Q(created_by=user_id)),many=True).data
                        
#                         elif ((start_date == None and end_date == None) and today != None):
#                             candidates_obj = CandidateCentralSerializer(CandidateCentral.objects.filter(Q(is_screening__gt=0) & Q(isarchive=False) & Q(created_date=today_date) & Q(created_by=user_id)),many=True).data
                        
#                         elif((start_date !=None and end_date!=None) and today== None):
#                             from_date = datetime.strptime(start_date,'%Y-%m-%d').date()
#                             to_date = datetime.strptime(end_date,'%Y-%m-%d').date()
#                             candidates_obj = CandidateCentralSerializer(CandidateCentral.objects.filter(Q(is_screening__gt=0) & Q(isarchive=False) & Q(created_date__range = [from_date,to_date]) & Q(created_by=user_id)),many=True).data
                        
#                         for candidate in candidates_obj:
#                             try:
#                                 designations = CandidateDesignation.objects.filter(candidate_id=candidate['id'])
#                                 for des in designations:
#                                     designation_name = Designation.objects.get(id=des.designation_id).name
#                                     candidate['designation'] = designation_name
#                             except:
#                                 candidate['designation'] = ""
#                             try:
#                                 skills = CandidateSkills.objects.filter(candidate_id=candidate['id'])
#                                 skill = []
#                                 for sk in skills:
#                                     skills_name = Skill.objects.get(
#                                         id=sk.skill_id).name
#                                     skill.append(skills_name)
#                                 candidate['skills'] = skill
#                             except:
#                                 candidate['skills'] = []
#                             output.append(candidate)
                        
#                     elif role == "Member":
#                         permission_obj = Permission.objects.get(member_id=user_id)
#                         if permission_obj.can_read == False :
#                            return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You don't have permission to list candidate"))
#                         output = []
#                         if (start_date != None and (end_date == None and today==None)):
#                             from_date = datetime.strptime(start_date,'%Y-%m-%d').date()
#                             candidates_obj = CandidateCentralSerializer(CandidateCentral.objects.filter(Q(is_screening__gt=0) & Q(isarchive=False) & Q(created_date__gte=from_date) & Q(created_by=user_id)),many=True).data
                        
#                         elif(end_date != None and (start_date == None and today==None)):
#                             to_date = datetime.strptime(end_date,'%Y-%m-%d').date()
#                             candidates_obj = CandidateCentralSerializer(CandidateCentral.objects.filter(Q(is_screening__gt=0) & Q(isarchive=False) & Q(created_date__lte=to_date) & Q(created_by=user_id)),many=True).data
#                             print("length",len(candidates_obj))
                        
#                         elif (start_date == None and end_date == None and today == None):
#                             candidates_obj = CandidateCentralSerializer(CandidateCentral.objects.filter(Q(is_screening__gt=0) & Q(isarchive=False) & Q(created_by=user_id)),many=True).data
                        
#                         elif ((start_date == None and end_date == None) and today != None):
#                             candidates_obj = CandidateCentralSerializer(CandidateCentral.objects.filter(Q(is_screening__gt=0) & Q(isarchive=False) & Q(created_date=today_date) & Q(created_by=user_id)),many=True).data
                        
#                         elif((start_date !=None and end_date!=None) and today== None):
#                             from_date = datetime.strptime(start_date,'%Y-%m-%d').date()
#                             to_date = datetime.strptime(end_date,'%Y-%m-%d').date()
#                             candidates_obj = CandidateCentralSerializer(CandidateCentral.objects.filter(Q(is_screening__gt=0) & Q(isarchive=False) & Q(created_date__range = [from_date,to_date]) & Q(created_by=user_id)),many=True).data
                        
#                         for candidate in candidates_obj:
#                             try:
#                                 designations = CandidateDesignation.objects.filter(candidate_id=candidate['id'])
#                                 for des in designations:
#                                     designation_name = Designation.objects.get(id=des.designation_id).name
#                                     candidate['designation'] = designation_name
#                             except:
#                                 candidate['designation'] = ""
#                             try:
#                                 skills = CandidateSkills.objects.filter(candidate_id=candidate['id'])
#                                 skill = []
#                                 for sk in skills:
#                                     skills_name = Skill.objects.get(
#                                         id=sk.skill_id).name
#                                     skill.append(skills_name)
#                                 candidate['skills'] = skill
#                             except:
#                                 candidate['skills'] = []
#                             output.append(candidate)
#                 else:
#                     return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, data=output, message="User Fetch successfully"))


# class TechFeedbackAPI(APIView):
#     def post(self, request, *args, **kwargs):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 if hr_obj.isactive:
#                     if role == 'User':
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are Not Authorized"))
#                     data = request.data
#                     print("870",data)
#                     candidate_id = data['candidate_id'] if 'candidate_id' in data.keys(
#                     ) else None
#                     print("873",candidate_id)
#                     round = data['round'] if 'round' in data.keys() else None
#                     print("875",round)
#                     feedback_map = payload_mapper(
#                         payload=data, modelField=feedback)
#                     print("878",feedback_map)
#                     candidate_obj = CandidateTechRoundReview.objects.filter(
#                         candidate_id=candidate_id, round=round)
#                     print("881",candidate_obj)
#                     if len(candidate_obj) == 0:
#                         if 'skill' in data.keys():
#                             for s in data['skill']:
#                                 obj = {
#                                     "candidate_id": candidate_id,
#                                     "skill_id": s['id'],
#                                     "rating": s['rating'],
#                                     "round": round,
#                                     "mode": request.data.get('mode'),
#                                     "created_date": current_date_time(),
#                                     "created_by": hr_id,
#                                     "modified_date": current_date_time(),
#                                     "modified_by": hr_id,
#                                 }
#                                 print('896',obj)
#                                 a=CandidateTechRoundReview.objects.create(**obj)
#                                 print("897",a)
#                         feedback_map['created_by'] = hr_id
#                         feedback_map['created_date'] = current_date_time()
#                         feedback_map['modified_by'] = hr_id
#                         feedback_map['modified_date'] = current_date_time()
#                         Feedback.objects.create(**feedback_map)
#                         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data created successfully", data=[]))
#                 else:
#                     return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))

#     def get(self, request, *args, **kwargs):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             hr_obj = Userlogin.objects.get(email=email)
#             if hr_obj:
#                 if hr_obj.isactive:
#                     id = request.query_params.get('id')
#                     candidate_tech_round_review_obj = CandidateTechRoundReview.objects.filter(
#                         candidate_id=id, round__in=[1, 2])
#                     candidate_serialized_review_obj = CandidateTechRoundReviewSerializer(
#                         candidate_tech_round_review_obj, many=True).data
#                     candidate_round_details = {}
#                     for round_review in candidate_serialized_review_obj:
#                         if round_review['round'] in candidate_round_details:
#                             skill_details = Skill.objects.get(
#                                 id=round_review['skill_id'])
#                             candidate_round_details[round_review['round']]['skills'].append(
#                                 {
#                                     "skill_name": skill_details.name,
#                                     "skill_id": skill_details.id,
#                                     "skill_rating": round_review["rating"]
#                                 })
#                         else:
#                             candidate_round_details[round_review['round']] = {}
#                             skill_details = Skill.objects.get(
#                                 id=round_review['skill_id'])
#                             candidate_round_details[round_review['round']
#                                                     ]['id'] = round_review['id']
#                             candidate_round_details[round_review['round']
#                                                     ]['candidate_id'] = round_review['candidate_id']
#                             candidate_round_details[round_review['round']
#                                                     ]['mode'] = round_review['mode']
#                             candidate_round_details[round_review['round']
#                                                     ]['created_date'] = round_review['created_date']
#                             candidate_round_details[round_review['round']]['comment'] = Feedback.objects.get(
#                                 candidate_id=id, stage=int(round_review['round'])+1).comment
#                             candidate_round_details[round_review['round']]['skills'] = [
#                                 {
#                                     "skill_name": skill_details.name,
#                                     "skill_id": skill_details.name,
#                                     "skill_rating": round_review["rating"]
#                                 }]
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data Fetched successfully", data=candidate_round_details))


# class UpdateCandidateStatus(APIView):
#     def post(self, request, *args, **kwargs):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 if hr_obj.isactive:
#                     if role == 'User':
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You Do not have permission to access this Page"))
#                     id = request.query_params.get('id')
#                     data = request.data
#                     candidate_obj = CandidateCentral.objects.get(
#                         Q(id=id) & Q(isarchive=False))
#                     if candidate_obj:
#                         candidate_interview_form = payload_mapper(
#                             payload=data, modelField=candidate_form)
#                         CandidateCentral.objects.filter(Q(id=id) & Q(
#                             isarchive=False)).update(**candidate_interview_form)
#                 else:
#                     return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Form Updated successfully", data=None))


# class GetDurapidEmployees(APIView):
#     def get(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             hr_obj = Userlogin.objects.get(email=email)
#             if hr_obj:
#                 if hr_obj.isactive:
#                     data = requests.get(
#                         url="https://hrdurapidbackend.azurewebsites.net/auth/employeeData")
#                     if data.status_code == 200:
#                         employee_objects = data.json()
#                         final_data = employee_objects['data']
#                         return JsonResponse(data_response(status=status.HTTP_200_OK, data=final_data, message="Data fetched successfully"))
#                     else:
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Employee data not Found"))
#                 else:
#                     return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))

# class ClientInterview(APIView):
#     def get(self, request):
#         try:
#             try:
#                 token = verified_token(request)
#                 email = token['email']
#                 hr_obj = Userlogin.objects.get(email=email)
#             except Exception as e:
#                 hr_obj = None
#             if hr_obj:
#                 if hr_obj.isactive:
#                     id = request.query_params.get('id')
#                     client_feedback = {}
#                     try:
#                         client_review = ClientFeedbackSerializer(
#                             ClientFeedback.objects.get(candidate_id=id)).data
#                     except Exception as e:
#                         client_review = None
#                     try:
#                         comment = Feedback.objects.get(
#                             candidate_id=id, stage=4).comment
#                     except Exception as e:
#                         comment = None
#                     client_feedback['client_review'] = client_review
#                     client_feedback['comment'] = comment
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data fetched successfully", data=client_feedback))

#     def post(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 if hr_obj.isactive:
#                     if role == 'User':
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are Not Authorized"))
#                     data = request.data
#                     candidate_id = data['candidate_id'] if 'candidate_id' in data.keys(
#                     ) else None
#                     feedback_map = payload_mapper(
#                         payload=data, modelField=feedback)
#                     candidate_obj = ClientFeedback.objects.filter(
#                         candidate_id=candidate_id)
#                     if len(candidate_obj) == 0:
#                         obj = {
#                             "candidate_id": candidate_id,
#                             "client_name": request.data.get('client_name'),
#                             "created_by": hr_id,
#                             "created_date": current_date_time(),
#                             "modified_by": hr_id,
#                             "modified_date": current_date_time()
#                         }
#                         ClientFeedback.objects.create(**obj)
#                         feedback_map['created_by'] = hr_id
#                         feedback_map['created_date'] = current_date_time()
#                         feedback_map['modified_by'] = hr_id
#                         feedback_map['modified_date'] = current_date_time()
#                         Feedback.objects.create(**feedback_map)
#                     return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data created successfully", data=[]))
#                 else:
#                     obj = {
#                         "client_name": request.data.get('client_name'),
#                         "modified_by": email,
#                         "modified_date": current_date_time()
#                     }
#                     ClientFeedback.objects.filter(
#                         candidate_id=candidate_id).update(**obj)
#                     feedback_map['modified_by'] = hr_id
#                     feedback_map['modified_date'] = current_date_time()
#                     Feedback.objects.filter(
#                         candidate_id=candidate_id, stage=4).update(**feedback_map)
#                     return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data Updated successfully", data=[]))
#             else:
#                 return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))


# class HrDiscussionAPI(APIView):
#     def get(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             hr_obj = Userlogin.objects.get(email=email)
#             if hr_obj:
#                 if hr_obj.isactive:
#                     id = request.query_params.get('id')
#                     hr_discussion = {}
#                     try:
#                         hr_discussion_obj = HrDiscussionSerializer(
#                             HrDiscussion.objects.get(candidate_id=id)).data
#                     except:
#                         hr_discussion_obj = None
#                     try:
#                         comment = Feedback.objects.get(
#                             candidate_id=id, stage=5).comment
#                     except Exception as e:
#                         comment = None
#                     hr_discussion['hr_discussion'] = hr_discussion_obj
#                     hr_discussion['comments'] = comment
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data fetched successfully", data=hr_discussion))

#     def post(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 if hr_obj.isactive:
#                     if role == 'User':
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You do not have permission to access this"))
#                     data = request.data
#                     candidate_id = data['candidate_id'] if 'candidate_id' in data.keys(
#                     ) else None
#                     candidate_obj = HrDiscussion.objects.filter(
#                         candidate_id=candidate_id)
#                     feedback_map = payload_mapper(
#                         payload=data, modelField=feedback)
#                     if len(candidate_obj) == 0:
#                         obj = {
#                             "candidate_id": candidate_id,
#                             "candidate_status": request.data.get('candidate_status'),
#                             "joining_date": request.data.get('joining_date'),
#                             "mode": request.data.get('mode'),
#                             "created_by": hr_id,
#                             "created_date": current_date_time(),
#                             "modified_by": hr_id,
#                             "modified_date": current_date_time()
#                         }
#                         HrDiscussion.objects.create(**obj)
#                         feedback_map['created_by'] = hr_id
#                         feedback_map['created_date'] = current_date_time()
#                         feedback_map['modified_by'] = hr_id
#                         feedback_map['modified_date'] = current_date_time()
#                         Feedback.objects.create(**feedback_map)
#                         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data created successfully", data=[]))
#                     else:
#                         obj = {
#                             "candidate_status": request.data.get('candidate_status'),
#                             "joining_date": request.data.get('joining_date'),
#                             "mode": request.data.get('mode'),
#                             "modified_by": hr_id,
#                             "modified_date": current_date_time()
#                         }
#                         HrDiscussion.objects.filter(
#                             candidate_id=candidate_id).update(**obj)
#                         feedback_map['modified_by'] = hr_id
#                         feedback_map['modified_date'] = current_date_time()
#                         Feedback.objects.filter(
#                             candidate_id=candidate_id, stage=5).update(**feedback_map)
#                         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data Updated successfully", data=[]))
#                 else:
#                     return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))


# class CandidateJoiningAPI(APIView):
#     def get(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             hr_obj = Userlogin.objects.get(email=email)
#             if hr_obj:
#                 if hr_obj.isactive:
#                     candidate_join = {}
#                     id = request.query_params.get('id')
#                     try:
#                         candidate_joining_obj = CandidateJoiningSerializer(
#                             CandidateJoining.objects.get(candidate_id=id)).data
#                     except Exception as e:
#                         candidate_joining_obj = None
#                     if candidate_joining_obj:
#                         comment = Feedback.objects.get(
#                             candidate_id=id, stage=6).comment
#                         candidate_join['candidate_obj'] = candidate_joining_obj
#                         candidate_join['comment'] = comment
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data fetched successfully", data=candidate_join))

#     def post(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 if hr_obj.isactive:
#                     if role == 'User':
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are Not Authorized"))
#                     data = request.data
#                     candidate_id = data['candidate_id'] if 'candidate_id' in data.keys(
#                     ) else None
#                     cand_obj = CandidateJoining.objects.filter(
#                         candidate_id=candidate_id)
#                     feedback_map = payload_mapper(
#                         payload=data, modelField=feedback)
#                     if len(cand_obj) == 0:
#                         obj = {
#                             "candidate_id": candidate_id,
#                             "offerletter_status": request.data.get('offerletter_status'),
#                             "joined_date": request.data.get('joined_date'),
#                             "created_by": hr_id,
#                             "created_date": current_date_time(),
#                             "modified_by": hr_id,
#                             "modified_date": current_date_time()
#                         }
#                         CandidateJoining.objects.create(**obj)
#                         feedback_map['created_by'] = hr_id
#                         feedback_map['created_date'] = current_date_time()
#                         feedback_map['modified_by'] = hr_id
#                         feedback_map['modified_date'] = current_date_time()
#                         Feedback.objects.create(**feedback_map)
#                         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data created successfully", data=[]))
#                     else:
#                         obj = {
#                             "offerletter_status": request.data.get('offerletter_status'),
#                             "joined_date": request.data.get('joined_date'),
#                             "modified_by": hr_id,
#                             "modified_date": current_date_time()
#                         }
#                         CandidateJoining.objects.filter(
#                             candidate_id=candidate_id).update(**obj)
#                         feedback_map['modified_by'] = hr_id
#                         feedback_map['modified_date'] = current_date_time()
#                         Feedback.objects.filter(
#                             candidate_id=candidate_id, stage=6).update(**feedback_map)
#                         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data Updated successfully", data=[]))
#             else:
#                 return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))


# class CandidateSoftSkillAPI(APIView):
#     def get(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             hr_obj = Userlogin.objects.get(email=email)
#             if hr_obj:
#                 if hr_obj.isactive:
#                     try:
#                         id = request.query_params.get('id')
#                         candidate_skills_obj = CandidateSoftskills.objects.filter(
#                             candidate_id=id)
#                         comment = Feedback.objects.get(
#                             candidate_id=id, stage=1).comment
#                         print("comment", comment)
#                         candidate_skills_serialized_obj = CandidateSoftskillsSerializer(
#                             candidate_skills_obj, many=True).data
#                         candidate_skills_arr = {}
#                         skill_data = []
#                         for k in candidate_skills_serialized_obj:
#                             k = dict(k)
#                             soft_skill = SoftSkills.objects.get(
#                                 id=k['soft_skill_id'])
#                             c = {"id": soft_skill.id, "name": soft_skill.name,
#                                  "rating": k['rating'], "created_date": k['created_date']}
#                             skill_data.append(c)
#                         candidate_skills_arr['skill_data'] = skill_data
#                         candidate_skills_arr['comment'] = comment
#                     except Exception as e:
#                         id = None
#                         candidate_skills_arr = {}
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data fetched successfully", data=[candidate_skills_arr]))

#     def post(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 if hr_obj.isactive:
#                     if role == "User":
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are Not Authorized"))
#                     data = request.data
#                     candidate_id = data['candidate_id'] if 'candidate_id' in data.keys(
#                     ) else None
#                     candidate_obj = CandidateSoftskills.objects.filter(
#                         candidate_id=candidate_id)
#                     feedback_map = payload_mapper(
#                         payload=data, modelField=feedback)
#                     if len(candidate_obj) == 0:
#                         if "soft_skill_id" in data.keys():
#                             for k in data['soft_skill_id']:
#                                 candidate_soft_skill = {
#                                     "candidate_id": request.data.get('candidate_id'),
#                                     "soft_skill_id": k['id'],
#                                     "created_by": hr_id,
#                                     "created_date": current_date_time(),
#                                     "rating": k['rating'],
#                                     "modified_by": hr_id,
#                                     "modified_date": current_date_time()
#                                 }
#                                 CandidateSoftskills.objects.create(
#                                     **candidate_soft_skill)
#                         feedback_map['created_by'] = hr_id
#                         feedback_map['created_date'] = current_date_time()
#                         feedback_map['modified_by'] = hr_id
#                         feedback_map['modified_date'] = current_date_time()
#                         Feedback.objects.create(**feedback_map)
#                         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data created successfully", data=[]))
#                     else:
#                         candidate_obj.delete()
#                         if "soft_skill_id" in data.keys():
#                             for k in data['soft_skill_id']:
#                                 candidate_soft_skill = {
#                                     "candidate_id": request.data.get('candidate_id'),
#                                     "soft_skill_id": k['id'],
#                                     "created_by": hr_id,
#                                     "created_date": current_date_time(),
#                                     "rating": k['rating'],
#                                     "modified_by": hr_id,
#                                     "modified_date": current_date_time()
#                                 }
#                                 CandidateSoftskills.objects.create(
#                                     **candidate_soft_skill)
#                         feedback_map['modified_by'] = hr_id
#                         feedback_map['modified_date'] = current_date_time()
#                         Feedback.objects.filter(
#                             candidate_id=candidate_id, stage=data['stage']).update(**feedback_map)
#                         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data Updated successfully", data=[]))
#                 else:
#                     return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))


# class TechnInviteAPI(APIView):
#     def post(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 if hr_obj.isactive:
#                     if role == 'User':
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are Not Authorized"))
#                     data = request.data
#                     candidate_id = data['candidate_id'] if 'candidate_id' in data.keys(
#                     ) else None
#                     round = data['round'] if 'round' in data.keys() else None
#                     candidate_obj = CandidateTechInterview.objects.filter(
#                         candidate_id=candidate_id, round=round)
#                     if len(candidate_obj) == 0:
#                         candidate_tech_invite = payload_mapper(
#                             payload=data, modelField=candidate_interview)
#                         candidate_logs = payload_mapper(
#                             payload=data, modelField=candidate_interview_history)

#                         candidate_tech_invite['created_by'] = hr_id
#                         candidate_tech_invite['created_date'] = current_date_time(
#                         )
#                         candidate_tech_invite['modified_by'] = hr_id
#                         candidate_tech_invite['modified_date'] = current_date_time(
#                         )

#                         candidate_logs['created_by'] = hr_id
#                         candidate_logs['created_date'] = current_date_time()
#                         candidate_logs['modified_by'] = hr_id
#                         candidate_logs['modified_date'] = current_date_time()
#                         CandidateTechInterview.objects.create(
#                             **candidate_tech_invite)
#                         CandidateTechInterviewLogs.objects.create(
#                             **candidate_logs)
#                         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data created successfully", data=None))
#                     else:
#                         candidate_tech_invite = payload_mapper(
#                             payload=data, modelField=candidate_interview)
#                         candidate_logs = payload_mapper(
#                             payload=data, modelField=candidate_interview_history)
#                         candidate_obj.delete()
#                         candidate_tech_invite['created_by'] = hr_id
#                         candidate_tech_invite['created_date'] = current_date_time(
#                         )
#                         candidate_tech_invite['modified_by'] = hr_id
#                         candidate_tech_invite['modified_date'] = current_date_time(
#                         )

#                         candidate_logs['created_by'] = hr_id
#                         candidate_logs['created_date'] = current_date_time()
#                         candidate_logs['modified_by'] = hr_id
#                         candidate_logs['modified_date'] = current_date_time()

#                         CandidateTechInterview.objects.create(
#                             **candidate_tech_invite)
#                         CandidateTechInterviewLogs.objects.create(
#                             **candidate_logs)
#             else:
#                 return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data updated successfully", data=None))

#     def get(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             hr_obj = Userlogin.objects.get(email=email)
#             if hr_obj:
#                 if hr_obj.isactive:
#                     id = request.query_params.get('id')
#                     candidate_obj = CandidateTechInterviewSerializer(
#                         CandidateTechInterview.objects.filter(candidate_id=id), many=True).data
#             else:
#                 return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data fetched successfully", data=candidate_obj))


# class JoinedAPI(APIView):
#     def get(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 if hr_obj.isactive:
#                     final_dict = {}
#                     id = request.query_params.get('id')
#                     comment = Feedback.objects.get(
#                         candidate_id=id, stage=7).comment
#                     candidate_obj = CandidateStatusSerializer(
#                         CandidateStatus.objects.get(candidate_id=id)).data
#                     final_dict['candidate_status'] = candidate_obj
#                     final_dict['comment'] = comment
#                 else:
#                     return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         except Exception as e:
#             return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data fetched successfully", data=final_dict))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data fetched successfully", data=final_dict))

#     def post(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 if hr_obj.isactive:
#                     if role == 'User':
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are Not Authorized"))
#                     data = request.data
#                     feedback_map = payload_mapper(
#                         payload=data, modelField=feedback)
#                     obj = {
#                         "candidate_id": data['candidate_id'],
#                         "is_joined": data['is_joined'],
#                         "created_by": hr_id,
#                         "created_date": current_date_time(),
#                         "modified_by": hr_id,
#                         "modified_date": current_date_time()
#                     }
#                     CandidateStatus.objects.create(**obj)
#                     feedback_map['created_by'] = hr_id
#                     feedback_map['created_date'] = current_date_time()
#                     feedback_map['modified_by'] = hr_id
#                     feedback_map['modified_date'] = current_date_time()
#                     Feedback.objects.create(**feedback_map)
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data created successfully", data=[]))


# class InterviewLogs(APIView):
#     def get(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 if hr_obj.isactive:
#                     id = request.query_params.get('id')
#                     candidate_logs = CandidateTechInterviewLogsSerializer(
#                         CandidateTechInterviewLogs.objects.filter(candidate_id=id), many=True).data
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data Fetched Successfully", data=candidate_logs))


# class SuggestedSkills(APIView):
#     def get(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             try:
#                 user_obj = Userlogin.objects.get(email=email)
#             except Exception as e:
#                 user_obj = TeamMembers.objects.get(email=email)
#             user_id = user_obj.id
#             if user_obj:
#                 if user_obj.isactive:
#                     res_skill_list = []
#                     id = request.query_params.get('id')
#                     designation_id = Designation.objects.get(id=id).id
#                     skill_names = Skill.objects.filter(
#                         skill_category=designation_id).values_list('name', flat=True)
#                     for skill in skill_names:
#                         res_skill_list.append({"id": Skill.objects.get(
#                             name=skill).id, "name": Skill.objects.get(name=skill).name})
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data fetched successfully", data=res_skill_list))


# class Dashboard(APIView):
#     def get(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             try:
#                 user_obj = Userlogin.objects.get(email=email)
#             except Exception as e:
#                 user_obj = TeamMembers.objects.get(email=email)
#             user_id = user_obj.id
#             name = user_obj.name
#             if user_obj:
#                 if user_obj.isactive:
#                     if role == 'User' or role == 'Admin':
#                         final_Output = {}
#                         candidate_resume_submitted = len(CandidateCentral.objects.filter(Q(isarchive=False)))
#                         final_Output['candidate_count'] = candidate_resume_submitted
#                         final_Output['HR_Name'] = name
#                         final_Output['Total_Candidate_on_HR_Round'] = len(Feedback.objects.filter(stage=1))
#                         final_Output['Total_Candidate_on_Technical_Round_1'] = len(Feedback.objects.filter(stage=2))
#                         final_Output['Total_Candidate_on_Technical_Round_2'] = len(Feedback.objects.filter(stage=3))
#                         final_Output['Total_Candidate_on_Client_Interview'] = len(Feedback.objects.filter(stage=4))
#                         final_Output['Total_Candidate_on_HR_Discussion'] = len(Feedback.objects.filter(stage=5))
#                         final_Output['Candidate_Joining'] = len(Feedback.objects.filter(stage=6))
#                         final_Output['Candidate_Joined_Status'] = len(Feedback.objects.filter(stage=7))
#                     elif role == "HR" or role=="Member":
#                         final_Output = {}
#                         candidate_resume_submitted = len(CandidateCentral.objects.filter(Q(created_by=user_id) & Q(isarchive=1)))
#                         final_Output['candidate_count'] = candidate_resume_submitted
#                         final_Output['HR_Name'] = name
#                         final_Output['Total_Candidate_on_HR_Round'] = len(Feedback.objects.filter(stage=1, created_by=user_id))
#                         final_Output['Total_Candidate_on_Technical_Round_1'] = len(Feedback.objects.filter(stage=2, created_by=user_id))
#                         final_Output['Total_Candidate_on_Technical_Round_2'] = len(Feedback.objects.filter(stage=3, created_by=user_id))
#                         final_Output['Total_Candidate_on_Client_Interview'] = len(Feedback.objects.filter(stage=3, created_by=user_id))
#                         final_Output['Total_Candidate_on_HR_Discussion'] = len(Feedback.objects.filter(stage=4, created_by=user_id))
#                         final_Output['Total_Candidate_on_wwd'] = len(Feedback.objects.filter(stage=5, created_by=user_id))
#                         final_Output['Total_Candidate_on_dwd'] = len(Feedback.objects.filter(stage=6, created_by=user_id))
#                         final_Output['Total_Candidate_on_dwwdw'] = len(Feedback.objects.filter(stage=7, created_by=user_id))
#                 else:
#                     return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Your account has been deactivated"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data fetched successfully", data=final_Output))


# class JobOpening(APIView):
#     def post(self,request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 if hr_obj.isactive:
#                     if role == 'Admin':
#                         data = request.data
#                         designation_id = data['designation']
#                         if Jobopenings.objects.filter(designation_id=designation_id).exists():
#                             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Job Opening is already listed"))
#                         job_opening = {
#                             "designation_id":data['designation'],
#                             "job_description":data['job_description'],
#                             "created_by":hr_id,
#                             "experience":data['experience'],
#                             "close_date":data['close_date'],
#                             "salary_range":data['salary_range'],
#                             "no_of_vacanies":data['no_of_vacanies'],
#                             "employment_type":data['employment_type'],
#                             "workmode":data['workmode'],
#                             "created_by":hr_id,
#                             "created_date":current_date_time(),
#                             "modified_by":hr_id,
#                             "modified_date":current_date_time()
#                         }
#                         job_data = Jobopenings.objects.create(**job_opening)
#                         job_serialized_data = JobOpeningSerializer(job_data).data
#                     else:
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Job Opening can only be add by Admin"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data fetched successfully", data=job_serialized_data))
    
#     def get(self,request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 if hr_obj.isactive:
#                     id = request.query_params.get('id')
#                     if id is not None:
#                         finalOp = {}
#                         job_serialized_data = JobOpeningSerializer(Jobopenings.objects.get(id=id)).data
#                         designation_id = Jobopenings.objects.get(id=id).designation_id
#                         listed_by = Jobopenings.objects.get(id=id).created_by
#                         finalOp['data'] = job_serialized_data
#                         name = Designation.objects.get(id=designation_id).name
#                         created_by = Userlogin.objects.get(id=listed_by).name
#                         finalOp['name'] = name
#                         finalOp['AddedBy'] = created_by
#                     else:
#                         finalOp = {'job_data':[]}
#                         job_serialized_data = JobOpeningSerializer(Jobopenings.objects.all(),many=True).data
#                         for job in job_serialized_data:
#                             job = dict(job)
#                             name = Designation.objects.get(id=job['designation_id']).name
#                             listed_by = Userlogin.objects.get(id=job['created_by']).name
#                             job['name'] = name
#                             job['AddedBy'] = listed_by
#                             finalOp['job_data'].append(job)
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data fetched successfully", data=finalOp))
    
#     def patch(self,request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 if hr_obj.isactive:
#                     if role!="Admin":
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Job Opening can only be updated by Admin"))
#                     data = request.data
#                     id = data['id']
#                     job_object = Jobopenings.objects.filter(id=id)
#                     print("job-object",job_object)
#                     if len(job_object) == 0:
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Data Does not exists"))
#                     else:
#                         job_data = {
#                         "id": id,
#                         "designation_id":data['designation'],
#                         "job_description":data['job_description'],
#                         "experience":data['experience'],
#                         "close_date":data['close_date'],
#                         "salary_range":data['salary_range'],
#                         "no_of_vacanies":data['no_of_vacanies'],
#                         "employment_type":data['employment_type'],
#                         "workmode":data['workmode'],
#                         "modified_by":hr_id,
#                         "modified_date":current_date_time()
#                         }
#                         job_object_data = Jobopenings.objects.filter(id=id).update(**job_data)
#                         job_serialized_object_data = JobOpeningSerializer(job_object_data).data
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data Updated successfully",data=None))
    
#     def delete(self,request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 if hr_obj.isactive:
#                     if role!="Admin":
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Job Opening can only be updated by Admin"))
#                     id = request.query_params.get('id')
#                     obj = Jobopenings.objects.get(id=id).delete()
#         except Exception as e:
#             print(f"{str(e)}")
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data Deleted successfully",data=None))


# class AddTeamMembers(APIView):
#     def post(self,request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 if hr_obj.isactive:
#                     if role == "HR":
#                         data = request.data
#                         email = data['email']
#                         if TeamMembers.objects.filter(email=email).exists():
#                             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Candidate Already exists"))
#                         user = {
#                             "name":data['name'],
#                             "isactive":data['isactive'],
#                             "email":email,
#                             "password":data['password'],
#                             "created_by":hr_id,
#                             "created_date":current_date_time(),
#                             "modified_by":hr_id,
#                             "modified_date":current_date_time()
#                         }
#                         user_obj = TeamMembers.objects.create(**user)
#                         id = TeamMembers.objects.get(email=email).id
#                         permisson_obj = {
#                             "member_id":id,
#                             "can_create":data['can_create'],
#                             "can_read":data['can_read'],
#                             "can_update":data['can_update'],
#                             "can_delete":data['can_delete'],
#                             "created_by":hr_id,
#                             "created_date":current_date_time(),
#                             "modified_by":hr_id,
#                             "modified_date":current_date_time()

#                         }
#                         Permission.objects.create(**permisson_obj)
#                     else:
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Only HR can add members"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data posted successfully", data=None))

#     def delete(self,request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 if hr_obj.isactive:
#                     id = request.query_params.get('id')
#                     if role != "HR":
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Only HR can delete members"))
#                     TeamMembers.objects.filter(id=id).delete()
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data Deleted successfully", data=None))

#     def get(self,request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 id = request.query_params.get('id')
#                 if hr_obj.isactive:
#                     if id is not None:
#                         finalOp = {"team_member_obj":[],"permission":[]}
#                         finalOp['team_member_obj'].append(TeamMembersSerializer(TeamMembers.objects.get(id=id)).data)
#                         finalOp['permission'].append(PermissionSerializer(Permission.objects.get(member_id=id)).data)
#                     else:
#                         finalOp = {"TeamObj":[]}
#                         Team_member_data = TeamMembersSerializer(TeamMembers.objects.all(),many=True).data
#                         for obj in Team_member_data:
#                             obj = dict(obj)
#                             team_member_id = TeamMembers.objects.get(id=obj['id']).id
#                             permission_obj = PermissionSerializer(Permission.objects.get(member_id=team_member_id)).data
#                             obj['permissions'] = permission_obj
#                             finalOp['TeamObj'].append(obj)
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data Fetched successfully", data=finalOp))
        
#     def patch(self,request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             hr_obj = Userlogin.objects.get(email=email)
#             hr_id = hr_obj.id
#             if hr_obj:
#                 if hr_obj.isactive:
#                     data = request.data
#                     id = data['id']
#                     user = {
#                         "id":id,
#                         "name":data['name'],
#                         "isactive":data['isactive'],
#                         "email":data['email'],
#                         "password":data['password'],
#                         "created_by":hr_id,
#                         "created_date":current_date_time(),
#                         "modified_by":hr_id,
#                         "modified_date":current_date_time(),
#                     }
#                     user_obj = TeamMembers.objects.filter(id=id).update(**user)
#                     permission_obj = {
#                         "member_id":id,
#                         "can_create":data['can_create'],
#                         "can_read":data['can_read'],
#                         "can_update":data['can_update'],
#                         "can_delete":data['can_delete'],
#                         "created_by":hr_id,
#                         "created_date":current_date_time(),
#                         "modified_by":hr_id,
#                         "modified_date":current_date_time()
#                     }
#                     permission_obj = Permission.objects.filter(member_id=id).update(**permission_obj)
                    
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data Updated successfully", data=None))


# from .serializers import CandidateCentralSerializer
    
class CandidateView(ModelViewSet):
    permission_classes = [IsAuthenticated and (IsAdmin or IsHR or IsMember)]
    queryset = CandidateCentral.objects.all()
    # serializer_class = CandidateCentralCreateSerializer

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CandidateCentralSerializer
        else:
            return CandidateCentralCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)


class CandidateSkillsView(ModelViewSet):
    queryset = CandidateSkills.objects.all()
    permission_classes = [IsAuthenticated and (IsAdmin or IsHR or IsMember)]


    def get_serializer_class(self):
        if self.request.method == "GET":
            return CandidateSkillsSerializer
        else:
            return CandidateSkillsCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)


class CandidateTechInterviewLogsView(ModelViewSet):
    queryset = CandidateTechInterviewLogs.objects.all()
    permission_classes = [IsAuthenticated and (IsAdmin or IsHR or IsMember)]

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CandidateTechInterviewLogsSerializer
        else:
            return CandidateTechInterviewLogsCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)


class CandidateSoftskillView(ModelViewSet):
    queryset = CandidateSoftskills.objects.all()
    permission_classes = [IsAuthenticated and (IsAdmin or IsHR or IsMember)]

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CandidateSoftskillsSerializer
        else:
            return CandidateSoftskillsCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)


class CandidateJoiningView(ModelViewSet):
    queryset = CandidateJoining.objects.all()
    permission_classes = [IsAuthenticated and (IsAdmin or IsHR or IsMember)]

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CandidateJoiningSerializer
        else:
            return CandidateJoiningCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)


class CandidateTechRoundReviewView(ModelViewSet):
    queryset = CandidateTechRoundReview.objects.all()
    permission_classes = [IsAuthenticated and (IsAdmin or IsHR or IsMember)]

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CandidateTechRoundReviewSerializer
        else:
            return CandidateTechRoundReviewCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)


class HrDisussionView(ModelViewSet):
    queryset = HrDiscussion.objects.all()
    permission_classes = [IsAuthenticated and (IsAdmin or IsHR or IsMember)]

    def get_serializer_class(self):
        if self.request.method == "GET":
            return HrDiscussionSerializer
        else:
            return HrDiscussionCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)


class ClientFeedbackView(ModelViewSet):
    queryset = ClientFeedback.objects.all()
    permission_classes = [IsAuthenticated and (IsAdmin or IsHR or IsMember)]

    def get_serializer_class(self):
        if self.request.method == "GET":
            return ClientFeedbackSerializer
        else:
            return ClientFeedbackCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)


class CandidateTechInterviewView(ModelViewSet):
    queryset = CandidateTechInterview.objects.all()
    permission_classes = [IsAuthenticated and (IsAdmin or IsHR or IsMember)]

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CandidateTechInterviewSerializer
        else:
            return CandidateTechInterviewCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)

class CandidateStatusView(ModelViewSet):
    queryset = CandidateStatus.objects.all()
    permission_classes = [IsAuthenticated and (IsAdmin or IsHR or IsMember)]

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CandidateStatusSerializer
        else:
            return CandidateStatusCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)

class FeedbackView(ModelViewSet):
    queryset = Feedback.objects.all()
    permission_classes = [IsAuthenticated and (IsAdmin or IsHR or IsMember)]

    def get_serializer_class(self):
        if self.request.method == "GET":
            return FeedbackSerializer
        else:
            return FeedbackCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)


class CandidateDesignationView(ModelViewSet):
    queryset = CandidateDesignation.objects.all()
    permission_classes = [IsAuthenticated and (IsAdmin or IsHR or IsMember)]

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CandidateDesignationSerializer
        else:
            return CandidateDesignationCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)

class CandidateCompanyView(ModelViewSet):
    queryset = CandidateCompany.objects.all()
    permission_classes = [IsAuthenticated and (IsAdmin or IsHR or IsMember)]

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CandidateCompanySerializer
        else:
            return CandidateCompanyCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)


class CandidateEducationView(ModelViewSet):
    queryset = CandidateEducation.objects.all()
    permission_classes = [IsAuthenticated and (IsAdmin or IsHR or IsMember)]

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CandidateEducationSerializer
        else:
            return CandidateEducationCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)



class CandidateCertificationView(ModelViewSet):
    queryset = CandidateCertification.objects.all()
    permission_classes = [IsAuthenticated and (IsAdmin or IsHR or IsMember)]

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CandidateCertificationSerializer
        else:
            return CandidateCertificationCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)


class CandidateLanguageView(ModelViewSet):
    queryset = CandidateLanguage.objects.all()
    permission_classes = [IsAuthenticated and (IsAdmin or IsHR or IsMember)]

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CandidateLanguageSerializer
        else:
            return CandidateLanguageCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)
