from rest_framework.serializers import ModelSerializer
from .models import *
from adminpanel.serializers import SkillSerializer


class CandidateCentralCreateSerializer(ModelSerializer):
    class Meta:
        model = CandidateCentral
        exclude = ('created_by','modified_by')

class CandidateCentralSerializer(ModelSerializer):
    class Meta:
        model = CandidateCentral
        fields = "__all__"
        

class CandidateSkillsCreateSerializer(ModelSerializer):
    class Meta:
        model = CandidateSkills
        exclude = ('created_by','modified_by')

class CandidateSkillsSerializer(ModelSerializer):
    candidate = CandidateCentralSerializer()
    skill = SkillSerializer()
    class Meta:
        model = CandidateSkills
        fields = "__all__"

    def to_representation(self, instance):
        reprsentation = super().to_representation(instance)
        candidate_obj = reprsentation['candidate']
        skill_obj = reprsentation['skill']
        candidate_info_obj = {
            "name" : candidate_obj.get('name'),
        }
        skill_info_obj = {"name":skill_obj.get('name')}
        reprsentation['candidate'] = candidate_info_obj
        reprsentation['skill'] = skill_info_obj
        return reprsentation


class CandidateTechInterviewLogsCreateSerializer(ModelSerializer):
    class Meta:
        model = CandidateTechInterviewLogs
        exclude = ('created_by','modified_by')

class CandidateTechInterviewLogsSerializer(ModelSerializer):
    class Meta:
        model = CandidateTechInterviewLogs
        fields = "__all__"

    
class CandidateSoftskillsCreateSerializer(ModelSerializer):
    class Meta:
        model = CandidateSoftskills
        exclude = ('created_by','modified_by')

class CandidateSoftskillsSerializer(ModelSerializer):
    class Meta:
        model = CandidateSoftskills
        fields = "__all__"

class CandidateJoiningCreateSerializer(ModelSerializer):
    class Meta:
        model = CandidateJoining
        exclude = ('created_by','modified_by')

class CandidateJoiningSerializer(ModelSerializer):
    class Meta:
        model = CandidateJoining
        fields = "__all__"

class CandidateTechRoundReviewCreateSerializer(ModelSerializer):
    class Meta:
        model = CandidateTechRoundReview
        exclude = ('created_by','modified_by')


class CandidateTechRoundReviewSerializer(ModelSerializer):
    class Meta:
        model = CandidateTechRoundReview
        fields = "__all__"

class HrDiscussionCreateSerializer(ModelSerializer):
    class Meta:
        model = HrDiscussion
        exclude = ('created_by','modified_by')

class HrDiscussionSerializer(ModelSerializer):
    class Meta:
        model = HrDiscussion
        fields = "__all__"


class ClientFeedbackCreateSerializer(ModelSerializer):
    class Meta:
        model = ClientFeedback
        exclude = ('created_by','modified_by')

class ClientFeedbackSerializer(ModelSerializer):
    class Meta:
        model = ClientFeedback
        fields = "__all__"


class CandidateTechInterviewCreateSerializer(ModelSerializer):
    class Meta:
        model = CandidateTechInterview
        exclude = ('created_by','modified_by')   

class CandidateTechInterviewSerializer(ModelSerializer):
    class Meta:
        model = CandidateTechInterview
        fields = "__all__"


class CandidateStatusCreateSerializer(ModelSerializer):
    class Meta:
        model = CandidateStatus
        exclude = ('created_by','modified_by') 

class CandidateStatusSerializer(ModelSerializer):
    class Meta:
        model = CandidateStatus
        fields = "__all__"


class FeedbackCreateSerializer(ModelSerializer):
    class Meta:
        model = Feedback
        exclude = ('created_by','modified_by') 

class FeedbackSerializer(ModelSerializer):
    class Meta:
        model = Feedback
        fields = "__all__"

class CandidateDesignationCreateSerializer(ModelSerializer):
    class Meta:
        model = CandidateDesignation
        exclude = ('created_by','modified_by') 

class CandidateDesignationSerializer(ModelSerializer):
    class Meta:
        model = CandidateDesignation
        fields = "__all__"

class CandidateDesignationCreateSerializer(ModelSerializer):
    class Meta:
        model = CandidateDesignation
        exclude = ('created_by','modified_by') 

class CandidateDesignationSerializer(ModelSerializer):
    class Meta:
        model = CandidateDesignation
        fields = "__all__"

class CandidateCompanyCreateSerializer(ModelSerializer):
    class Meta:
        model = CandidateCompany
        exclude = ('created_by','modified_by') 

class CandidateCompanySerializer(ModelSerializer):
    class Meta:
        model = CandidateCompany
        fields = "__all__"

class CandidateEducationCreateSerializer(ModelSerializer):
    class Meta:
        model = CandidateEducation
        exclude = ('created_by','modified_by') 

class CandidateEducationSerializer(ModelSerializer):
    class Meta:
        model = CandidateEducation
        fields = "__all__"

class CandidateCertificationCreateSerializer(ModelSerializer):
    class Meta:
        model = CandidateCertification
        exclude = ('created_by','modified_by') 

class CandidateCertificationSerializer(ModelSerializer):
    class Meta:
        model = CandidateCertification
        fields = "__all__"

class CandidateLanguageCreateSerializer(ModelSerializer):
    class Meta:
        model = CandidateLanguage
        exclude = ('created_by','modified_by') 

class CandidateLanguageSerializer(ModelSerializer):
    class Meta:
        model = CandidateLanguage
        fields = "__all__"







