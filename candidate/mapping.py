candidate_form = {
      "name": "name",
      "email":"email",
      "mobile_no": "mobile_no",
      "experience_year":"experience_year",
      "experience_months":"experience_months",
      "address":"address",
      "ectc":"ectc",
      "cctc":"cctc",
      "notice_period_days":"notice_period_days",
      "is_screening":"is_screening",
      "city":"city",
      "state":"state",
      "notice_period_months":"notice_period_months",
      "preferred_location":"preferred_location",
      "resume_link":"resume_link",
      "stage":"stage",
}

feedback_form = {
      "stage":"stage",
      "candidate_id":"candidate_id",
      "comment":"comment",
      "is_interviewed":"is_interviewed",
}

candidate_interview= {
      "candidate_id":"candidate_id",
      "interviewer_id":"interviewer_id",
      "invite_time":"invite_time",
      "invite_date":"invite_date",
      "invite_link":"invite_link",
      "reason":"reason",
      "is_interviewed":"is_interviewed",
      "round":"round",
      "note":"note"

}

candidate_interview_history = {
      "candidate_id":"candidate_id",
      "interviewer_id":"interviewer_id",
      "invite_time":"invite_time",
      "invite_date":"invite_date",
      "invite_link":"invite_link",
      "reason":"reason",
      "round":"round",
      "is_interviewed":"is_interviewed",
      "note":"note",
      "stage":"stage"
}

feedback = {
      "stage":"stage",
      "candidate_id":"candidate_id",
      "comment":"comment",
      "interviewer_id":"interviewer_id",
      "interviewer_name":"interviewer_name",
}