from django.contrib import admin
# Register your models here.

# @admin.register(CandidateCentral)
# class CandidateCentralAdmin(admin.ModelAdmin):
#     list_display = [field.name for field in CandidateCentral._meta.fields]

from .models import CandidateCentral, CandidateCompany, CandidateDesignation, CandidateTechRoundReview, CandidateEducation, CandidateReference, CandidateJobtype, CandidateSkills, CandidateStatus, CandidateCertification, CandidateLanguage, Feedback, CandidateInterview, CandidateSoftskills, ClientFeedback, HrDiscussion, CandidateJoining, CandidateTechInterviewLogs, CandidateTechInterview


@admin.register(CandidateCentral)
class CandidateCentralAdmin(admin.ModelAdmin):
    list_display = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'id',
        'name',
        'email',
        'mobile_no',
        'experience_year',
        'experience_months',
        'ectc',
        'cctc',
        'notice_period_days',
        'is_screening',
        'stage',
        'notice_period_months',
        'resume_link',
        'isarchive',
        'state',
        'city',
        'master_candidate_id',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
    )
    search_fields = ('name',)


@admin.register(CandidateCompany)
class CandidateCompanyAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'company',
        'isactive',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'company',
    )


@admin.register(CandidateDesignation)
class CandidateDesignationAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'designation',
        'candidate',
        'isactive',
        'isapproved',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'designation',
        'candidate',
    )


@admin.register(CandidateTechRoundReview)
class CandidateTechRoundReviewAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'skill_id',
        'rating',
        'round',
        'mode',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
    )


@admin.register(CandidateEducation)
class CandidateEducationAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'education',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'education',
    )


@admin.register(CandidateReference)
class CandidateReferenceAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'reference',
        'isactive',
        'isapproved',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'reference',
    )


@admin.register(CandidateJobtype)
class CandidateJobtypeAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'job_type',
        'isactive',
        'isapproved',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'job_type',
    )


@admin.register(CandidateSkills)
class CandidateSkillsAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'skill',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'skill',
    )


@admin.register(CandidateStatus)
class CandidateStatusAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'is_joined',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
    )


@admin.register(CandidateCertification)
class CandidateCertificationAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'certification',
        'isactive',
        'isapproved',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'certification',
    )


@admin.register(CandidateLanguage)
class CandidateLanguageAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'language',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'language',
    )


@admin.register(Feedback)
class FeedbackAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'stage',
        'candidate',
        'comment',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
    )


@admin.register(CandidateInterview)
class CandidateInterviewAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'interviewer_id',
        'invite_time',
        'invite_date',
        'invite_link',
        'reason',
        'is_interviewed',
        'status',
        'comment',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
    )


@admin.register(CandidateSoftskills)
class CandidateSoftskillsAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'soft_skill',
        'rating',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'soft_skill',
    )


@admin.register(ClientFeedback)
class ClientFeedbackAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'client_name',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
    )


@admin.register(HrDiscussion)
class HrDiscussionAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'candidate_status',
        'joining_date',
        'mode',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'joining_date',
    )


@admin.register(CandidateJoining)
class CandidateJoiningAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'offerletter_status',
        'joined_date',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'joined_date',
    )


@admin.register(CandidateTechInterviewLogs)
class CandidateTechInterviewLogsAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
        'interviewer_id',
        'invite_time',
        'invite_date',
        'note',
    )
    list_filter = (
        'created_date',
        'modified_date',
        'created_by',
        'modified_by',
        'candidate',
    )