from django.urls import path
from .views import *

from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register('candidate',CandidateView,basename='candidate')
router.register('candidate-skill',CandidateSkillsView,basename='candidate_skill')
router.register('candidate-soft-skill',CandidateSoftskillView,basename='candidate_soft_skill')
router.register('candidate-joining',CandidateJoiningView,basename='candidate_joining')
router.register('candidate-tech-interview',CandidateTechInterviewView,basename='candidate_tech_interview')
router.register('candidate-status',CandidateStatusView,basename='candidate_status')
router.register('candidate-education',CandidateEducationView,basename='candidate_education')
router.register('candidate-certification',CandidateCertificationView,basename='candidate_certification')
router.register('candidate-company',CandidateCompanyView,basename='candidate_company')
router.register('candidate-language',CandidateLanguageView,basename='candidate_language')
router.register('candidate-tech-round-review',CandidateTechRoundReviewView,basename='candidate_tech_round_review')
router.register('candidate-tech-interview-logs',CandidateTechInterviewLogsView,basename='candidate_tech_interview_logs')
router.register('hr-discussion',HrDisussionView,basename='hr_discussion')
router.register('client-feedback',ClientFeedbackView,basename='client_feedback')
router.register('feedback',FeedbackView,basename='feedback')

urlpatterns = [
    # path('upload',ResumeParseAPI.as_view()),
    # path('post_candidate_data',PostCandidateForm.as_view()), #----> Done
    # path('candidate_list',ListCandidates.as_view()), #------> Pending ---> Need to be discussed
    # path('edit_candidate',EditCandidate.as_view()), #------> Done
    # path('get_candidate',GetCandidateInfo.as_view()), #------> Done
    # path('delete_user',DeleteUserObj.as_view()), #------> Done
    # path('screening_user',GetCandidateInfoInterview.as_view()), #------> Done
    # path('feedback',TechFeedbackAPI.as_view()),
    # path('status_update',UpdateCandidateStatus.as_view()),  
    # path('employee_list',GetDurapidEmployees.as_view()),
    # path('client_interview',ClientInterview.as_view()),
    # path('hr_discussion',HrDiscussionAPI.as_view()),
    # path('candidate_joining',CandidateJoiningAPI.as_view()),
    
    # path('candidate_softskills',CandidateSoftSkillAPI.as_view()),
    # path('techinvite',TechnInviteAPI.as_view()),
    # path('interview_logs',InterviewLogs.as_view()),
    # path('joining',JoinedAPI.as_view()),
    # path('suggested_skills',SuggestedSkills.as_view()), 
    # path('',Dashboard.as_view()),
    # path('job_openings',JobOpening.as_view()),  
    # path('member',AddTeamMembers.as_view()),  
]

urlpatterns += router.urls
