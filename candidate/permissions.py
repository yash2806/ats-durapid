from rest_framework.permissions import BasePermission
from login.models import Roles

class IsAdmin(BasePermission):
    def has_permission(self, request, view):
        return Roles.ADMIN.value in request.user.role

class IsHR(BasePermission):
    def has_permission(self, request, view):
        return Roles.HR.value in request.user.role

class IsMember(BasePermission):
    def has_permission(self, request, view):
        return Roles.MEMBER.value in request.user.role
