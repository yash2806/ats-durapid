from django.core.mail import EmailMessage

email = EmailMessage(
    'Subject Here',
    'Here is the message.',
    'from@example.com',
    ['to@example.com'],
    ['bcc@example.com'],
    reply_to=['another@example.com'],
    headers={'Message-ID': 'foo'},
)

email.send()