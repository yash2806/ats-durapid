from __helpers.jwtsign import decode_jwt
from datetime import datetime
from __helpers.responce import success_response,failure_response


def current_date_time():
    now = datetime.now()
    return now

def current_time():
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    return str(current_time)

def current_date():
    now = datetime.now()
    dt_string = now.strftime('%d-%m-%Y')
    date = dt_string.replace("/","-")
    return date


def verified_token(request):
    token = decode_jwt(request.META.get('HTTP_AUTHORIZATION'))
    print('token',token)
    return token

def payload_mapper(payload,modelField):
    try:
        modelFieldKeys = modelField.keys()
        modelFieldWithValues = {}
        for key in modelFieldKeys:
            try:
                modelFieldWithValues[key] = payload[modelField[key]]
            except Exception as e:
                pass
    except Exception as e:
        return {}
    return modelFieldWithValues


