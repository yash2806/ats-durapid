from django.db import models
from django_currentuser.middleware import (
    get_current_user, get_current_authenticated_user)
from django.contrib.auth import get_user_model

from login.models import User
from django_currentuser.db.models import CurrentUserField

# Create your models here.
class BaseModel(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True,null=True,blank=True)
    created_by = models.ForeignKey(User,related_name='%(class)s_createdby',on_delete=models.CASCADE,null=True,blank=True)
    modified_by = models.ForeignKey(User,related_name='%(class)s_modifiedby',null=True,blank=True,on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        user = get_current_authenticated_user()
        print('base model user =', user)

        if user is not None:
            self.modified_by = user
            if not self.id:
                self.created_by = user
        super(BaseModel, self).save(*args, **kwargs)

    def __str__(self):
        try:
            return self.created_by if self.created_by else '-'
        except:
            return '-'

    class Meta:
        abstract = True