from django.contrib import admin
from .models import * 
# Register your models here.


admin.site.register(Skill)
admin.site.register(Company)
admin.site.register(Designation)
admin.site.register(Education)
admin.site.register(GetReference)
admin.site.register(Certification)
admin.site.register(Language)
admin.site.register(SoftSkills)
admin.site.register(Jobopenings)
