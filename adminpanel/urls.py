from django.urls import path
from .views import * 
from rest_framework.routers import DefaultRouter

router = DefaultRouter()


router.register('skills',ListSkills,basename='skill')
router.register('company',ListCompany,basename='company')
router.register('jobtype',JobTypeAPI,basename='jobtype')
router.register('certificates',ListCertificates,basename='certificates')
router.register('reference',ListReferences,basename='reference')
router.register('designation',ListDesignation,basename='designation')
router.register('language',ListDesignation,basename='language')
router.register('education',ListEducation,basename='education')
router.register('softskills',SoftSkillsAPI,basename='softskills')
router.register('jobopenings',JobOpening,basename='jobopenings')



urlpatterns = [
    # path('skills',ListSkills.as_view()),
    # path('company',ListCompany.as_view()),
    # path('jobtype',JobTypeAPI.as_view()),
    # path('reference',ListReferences.as_view()),
    # path('certificates',ListCertificates.as_view()),
    # path('designation',ListDesignation.as_view()),
    # path('language',ListLanguage.as_view()),
    # path('education',ListEducation.as_view()),
    # path('softskills',SoftSkillsAPI.as_view()),
]

urlpatterns += router.urls

    