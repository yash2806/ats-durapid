from django.db import models
from hiring_process.models import BaseModel
# Create your models here.
class Company(BaseModel):
    name = models.CharField(max_length=255, blank=True, null=True)
    
class Designation(BaseModel):
    name = models.CharField(max_length=255, blank=True, null=True)

class Education(BaseModel):
    name = models.CharField(max_length=255, blank=True, null=True)

class GetReference(BaseModel):
    name = models.CharField(max_length=255, blank=True, null=True)

class JobType(BaseModel):
    name = models.CharField(max_length=255, blank=True, null=True)


class Skill(BaseModel):
    name = models.CharField(max_length=255, blank=True, null=True)


class Certification(BaseModel):
    name = models.CharField(max_length=255, blank=True, null=True)

class Language(BaseModel):
    name = models.CharField(max_length=255, blank=True, null=True)

class SoftSkills(BaseModel):
    name = models.CharField(max_length=255, blank=True, null=True)

class Jobopenings(BaseModel):
    designation_id = models.IntegerField(blank=True, null=True)
    job_description = models.CharField(max_length=255, blank=True, null=True)
    no_of_vacanies = models.IntegerField(blank=True, null=True)
    experience = models.CharField(max_length=255, blank=True, null=True)
    close_date = models.DateTimeField(blank=True, null=True)
    salary_range = models.CharField(max_length=255, blank=True, null=True)
    employment_type = models.CharField(max_length=255, blank=True, null=True)
    workmode = models.CharField(max_length=255, blank=True, null=True)