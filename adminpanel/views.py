from django.shortcuts import render
from .models import * 
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from .serializers import *
from login.models import * 
from __helpers.responce import * 
from rest_framework import status
from .permissions import IsAdmin, IsHR, IsMember
# Create your views here.


class ListSkills(ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdmin]
    # serializer_class = SkillsSerializer
    queryset = Skill.objects.all()

    def get_serializer_class(self):
        if self.request.method == "GET":
            return SkillSerializer
        else:
            return SkillsCreateSerializer

    # def perform_create(self, serializer_class):
    #     return self.serializer_class.save()
    def perform_create(self,serializer):
        return super().perform_create(serializer)

class ListCompany(ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdmin]
    queryset = Company.objects.all()

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CompanySerializer
        else:
            return CompanyCreateSerializer

    def perform_create(self,serializer):
        return super().perform_create(serializer)

class JobTypeAPI(ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdmin]
    queryset = JobType.objects.all()

    def get_serializer_class(self):
        if self.request.method == "GET":
            return JobTypeSerializer
        else:
            return JobTypeCreateSerializer

    def perform_create(self,serializer):
        return super().perform_create(serializer)


class ListCertificates(ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdmin]
    queryset = Certification.objects.all()

    def get_serializer_class(self):
        if self.request.method == "GET":
            return CertificationSerializer
        else:
            return CertificationCreateSerializer

    def perform_create(self,serializer):
        return super().perform_create(serializer)


class ListReferences(ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdmin]
    queryset = GetReference.objects.all()

    def get_serializer_class(self):
        if self.request.method == "GET":
            return GetReferenceSerializer
        else:
            return GetReferenceCreateSerializer

    def perform_create(self,serializer):
        return super().perform_create(serializer)

class ListDesignation(ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdmin]
    queryset = Designation.objects.all()

    def get_serializer_class(self):
        if self.request.method == "GET":
            return DesignationSerializer
        else:
            return DesignationCreateSerializer

    def perform_create(self,serializer):
        return super().perform_create(serializer)


class ListLanguage(ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdmin]
    queryset = Language.objects.all()

    def get_serializer_class(self):
        if self.request.method == "GET":
            return LanguageSerializer
        else:
            return LanguageCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)

class ListEducation(ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdmin]
    queryset = Education.objects.all()

    def get_serializer_class(self):
        if self.request.method == "GET":
            return EducationSerializer
        else:
            return EducationCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)


class SoftSkillsAPI(ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdmin]
    queryset = SoftSkills.objects.all()

    def get_serializer_class(self):
        if self.request.method == "GET":
            return SoftSkillsSerializer
        else:
            return SoftSkillsCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)


class JobOpening(ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdmin]
    queryset = Jobopenings.objects.all()

    def get_serializer_class(self):
        if self.request.method == "GET":
            return JobOpeningSerializer
        else:
            return JobOpeningCreateSerializer

    def perform_create(self,serializer):
         return super().perform_create(serializer)









# class JobTypeAPI(APIView):
    def get(self, request):
        try:
            token = verified_token(request)
            email = token['email']
            try:
                user_obj = Userlogin.objects.get(email=email)
            except Exception as e:
                user_obj = TeamMembers.objects.get(email=email)
            if user_obj:
                if user_obj.isactive:
                    job_type = JobTypeSerializer(
                        JobType.objects.all(), many=True).data
                else:
                    return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
            else:
                return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Something went wrong"))
        except Exception as e:
            return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
        return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data fetched successfully", data=job_type))

    def post(self, request):
        try:
            token = verified_token(request)
            email = token['email']
            role = token['role']
            try:
                user_obj = Userlogin.objects.get(email=email)
            except Exception as e:
                user_obj = TeamMembers.objects.get(email=email)
            user_id = user_obj.id
            if user_obj:
                if user_obj.isactive:
                    if role == 'User' or role == "Member":
                        return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are Not Authorized"))
                    name = request.data['name'].capitalize()
                    if JobType.objects.filter(name=name).exists():
                        return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Job Type Already Exists"))
                    else:
                        job_type = {
                            "name": name,
                            "isactive": 1,
                            "created_by": user_id,
                            "created_date": current_date_time(),
                            "modified_by": user_id,
                            "modified_date": current_date_time(),
                        }
                        job_type = JobType.objects.create(**job_type)
                        job_type_serialized_obj = JobTypeSerializer(
                            job_type).data
            else:
                return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
        except Exception as e:
            return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
        return JsonResponse(data_response(status=status.HTTP_201_CREATED, message="Data created successfully", data=job_type_serialized_obj))


# class ListReferences(APIView):
#     def get(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             try:
#                 user_obj = Userlogin.objects.get(email=email)
#             except Exception as e:
#                 user_obj = TeamMembers.objects.get(email=email)
#             if user_obj:
#                 if user_obj.isactive:
#                     reference_obj = GetReferenceSerializer(
#                         GetReference.objects.all(), many=True).data
#                 else:
#                     return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data fetched successfully", data=reference_obj))

#     def post(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             try:
#                 user_obj = Userlogin.objects.get(email=email)
#             except Exception as e:
#                 user_obj = TeamMembers.objects.get(email=email)
#             user_id = user_obj.id
#             if user_obj:
#                 if user_obj.isactive:
#                     if role == 'User' or role == "Member":
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are Not Authorized"))
#                     name = request.data['name'].capitalize()
#                     if GetReference.objects.filter(name=name).exists():
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Reference type Already Exists"))
#                     else:
#                         reference = {
#                             "name": name,
#                             "isactive": 1,
#                             "created_by": user_id,
#                             "created_date": current_date_time(),
#                             "modified_by": user_id,
#                             "modified_date": current_date_time(),
#                         }
#                         reference = GetReference.objects.create(**reference)
#                         reference_serialized_obj = GetReferenceSerializer(
#                             reference).data
#                 else:
#                     return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_201_CREATED, message="Data created successfully", data=reference_serialized_obj))

# class ListDesignation(APIView):
#     def get(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             try:
#                 user_obj = Userlogin.objects.get(email=email)
#             except Exception as e:
#                 user_obj = TeamMembers.objects.get(email=email)
#             if user_obj:
#                 if user_obj.isactive:
#                     designation_obj = DesignationSerializer(
#                         Designation.objects.all(), many=True).data
#                 else:
#                     return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data fetched successfully", data=designation_obj))

#     def post(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             try:
#                 user_obj = Userlogin.objects.get(email=email)
#             except Exception as e:
#                 user_obj = TeamMembers.objects.get(email=email)
#             user_id = user_obj.id
#             if user_obj:
#                 if user_obj.isactive:
#                     if role == 'User' or role == "Member":
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are Not Authorized"))
#                     name = request.data['name'].capitalize()
#                     if Designation.objects.filter(name=name).exists():
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Designation Already Exists"))
#                     else:
#                         designation = {
#                             "name": name,
#                             "isactive": 1,
#                             "created_by": user_id,
#                             "created_date": current_date_time(),
#                             "modified_by": user_id,
#                             "modified_date": current_date_time(),
#                         }
#                         designation = Designation.objects.create(**designation)
#                         designation_serialized_obj = DesignationSerializer(
#                             designation).data
#                 else:
#                     return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_201_CREATED, message="Data created successfully", data=designation_serialized_obj))

# class ListLanguage(APIView):
#     def get(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             try:
#                 user_obj = Userlogin.objects.get(email=email)
#             except Exception as e:
#                 user_obj = TeamMembers.objects.get(email=email)
#             if user_obj:
#                 if user_obj.isactive:
#                     language_obj = LanguageSerializer(
#                         Language.objects.all(), many=True).data
#                 else:
#                     return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Something went wrong"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data fetched successfully", data=language_obj))

#     def post(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             try:
#                 user_obj = Userlogin.objects.get(email=email)
#             except Exception as e:
#                 user_obj = TeamMembers.objects.get(email=email)
#             user_id = user_obj.id
#             if user_obj:
#                 if user_obj.isactive:
#                     if role == 'User' or role == "Member":
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are Not Authorized"))
#                     name = request.data['name'].capitalize()
#                     if Language.objects.filter(name=name).exists():
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Language Already Exists"))
#                     else:
#                         language = {
#                             "name": name,
#                             "isactive": 1,
#                             "isapproved": 1,
#                             "created_by": user_id,
#                             "created_date": current_date_time(),
#                             "modified_by": user_id,
#                             "modified_date": current_date_time(),
#                         }
#                         language = Language.objects.create(**language)
#                         language_serialized_obj = LanguageSerializer(
#                             language).data
#                 else:
#                     return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Something went wrong"))
#         return JsonResponse(data_response(status=status.HTTP_201_CREATED, message="Data created successfully", data=language_serialized_obj))

# class ListEducation(APIView):
#     def get(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             try:
#                 user_obj = Userlogin.objects.get(email=email)
#             except Exception as e:
#                 user_obj = TeamMembers.objects.get(email=email)
#             if user_obj:
#                 if user_obj.isactive:
#                     education_obj = EducationSerializer(
#                         Education.objects.all(), many=True).data
#                 else:
#                     return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data fetched successfully", data=education_obj))

#     def post(self, request):
#         try:
#             token = verified_token(request)
#             email = token['email']
#             role = token['role']
#             try:
#                 user_obj = Userlogin.objects.get(email=email)
#             except Exception as e:
#                 user_obj = TeamMembers.objects.get(email=email)
#             user_id = user_obj.id
#             if user_obj:
#                 if user_obj.isactive:
#                     if role == 'User' or role == "Member":
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are Not Authorized"))
#                     name = request.data['name'].capitalize()
#                     if Education.objects.filter(name=name).exists():
#                         return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="Education already exists"))
#                     else:
#                         education = {
#                             "name": name,
#                             "created_by": user_id,
#                             "created_date": current_date_time(),
#                             "modified_by": user_id,
#                             "modified_date": current_date_time(),
#                         }
#                         education = Education.objects.create(**education)
#                         ed_serialized_obj = EducationSerializer(education).data
#                 else:
#                     return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
#         except Exception as e:
#             return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
#         return JsonResponse(data_response(status=status.HTTP_201_CREATED, message="Data created successfully", data=ed_serialized_obj))

# class SoftSkillsAPI(APIView):
    def get(self, request):
        try:
            token = verified_token(request)
            email = token['email']
            hr_obj = Userlogin.objects.get(email=email)
            if hr_obj:
                if hr_obj.isactive:
                    soft_skills = SoftSkillsSerializer(
                        SoftSkills.objects.all(), many=True).data
            else:
                return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
        except Exception as e:
            return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
        return JsonResponse(data_response(status=status.HTTP_200_OK, message="Data fetched successfully", data=soft_skills))

    def post(self, request):
        try:
            token = verified_token(request)
            email = token['email']
            role = token['role']
            hr_obj = Userlogin.objects.get(email=email)
            hr_id = hr_obj.id
            if hr_obj:
                if hr_obj.isactive:
                    if role == 'User':
                        return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are Not Authorized"))
                    soft_skills_obj = {
                        "name": request.data['name'].capitalize(),
                        "created_by": hr_id,
                        "created_date": current_date_time(),
                        "modified_by": hr_id,
                        "modified_date": current_date_time(),
                    }
                    soft_skills = SoftSkills.objects.create(**soft_skills_obj)
                else:
                    return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message="You are not authorized to access this"))
        except Exception as e:
            return JsonResponse(failure_response(status=status.HTTP_400_BAD_REQUEST, message=f"{str(e)}"))
        return JsonResponse(data_response(status=status.HTTP_201_CREATED, message="Data created successfully", data=None))



