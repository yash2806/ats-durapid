from rest_framework.serializers import ModelSerializer
from .models import *


class SkillsCreateSerializer(ModelSerializer):
    class Meta:
        model = Skill
        exclude = ('created_by', 'modified_by',)

class SkillSerializer(ModelSerializer):
    class Meta:
        model = Skill
        fields = "__all__"

class CompanyCreateSerializer(ModelSerializer):
    class Meta:
        model = Company
        exclude = ('created_by', 'modified_by',)

class CompanySerializer(ModelSerializer):
    class Meta:
        model = Company
        fields = "__all__"
   

class JobTypeCreateSerializer(ModelSerializer):
    class Meta:
        model = JobType
        exclude = ('created_by', 'modified_by',)

class JobTypeSerializer(ModelSerializer):
    class Meta:
        model = JobType
        fields = "__all__"

class GetReferenceCreateSerializer(ModelSerializer):
    class Meta:
        model = GetReference
        exclude = ('created_by', 'modified_by',)

class GetReferenceSerializer(ModelSerializer):
    class Meta:
        model = GetReference
        fields = "__all__"

class CertificationCreateSerializer(ModelSerializer):
    class Meta:
        model = Certification
        exclude = ('created_by', 'modified_by',)


class CertificationSerializer(ModelSerializer):
    class Meta:
        model = Certification
        fields = '__all__'


class DesignationCreateSerializer(ModelSerializer):
    class Meta:
        model = Designation
        exclude = ('created_by', 'modified_by',)

class DesignationSerializer(ModelSerializer):
    class Meta:
        model = Designation
        fields = "__all__"


class LanguageCreateSerializer(ModelSerializer):
    class Meta:
        model = Language
        exclude = ('created_by', 'modified_by',)


class LanguageSerializer(ModelSerializer):
    class Meta:
        model = Language
        fields = "__all__"


class EducationCreateSerializer(ModelSerializer):
    class Meta:
        model = Education
        exclude = ('created_by', 'modified_by',)


class EducationSerializer(ModelSerializer):
    class Meta:
        model = Education
        fields = "__all__"


class SoftSkillsCreateSerializer(ModelSerializer):
    class Meta:
        model = SoftSkills
        exclude = ('created_by', 'modified_by',)

class SoftSkillsSerializer(ModelSerializer):
    class Meta:
        model = SoftSkills
        fields = "__all__"


class JobOpeningCreateSerializer(ModelSerializer):
    class Meta:
        model = Jobopenings
        exclude = ('created_by', 'modified_by',)

class JobOpeningSerializer(ModelSerializer):
    class Meta:
        model = Jobopenings
        fields = "__all__"
