from django.contrib import admin
from .models import User
# Register your models here.
admin.site.register(User)

values = [field.name for field in User._meta.fields]