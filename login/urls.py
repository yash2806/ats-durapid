from django.urls import path
from .views import HRLoginView, UserViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
# router.register('user',HRLoginView,basename='login')
router.register('user',UserViewSet,basename='user')
urlpatterns = [
    path('userlogin/',HRLoginView.as_view(),name='login'),
]

urlpatterns += router.urls

