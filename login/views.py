from __helpers.jwtsign import create_jwt,decode_jwt
from __helpers.responce import failure_response, data_response
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import status
from login.models import User
# from candidate.serializers import PermissionSerializer
from rest_framework import viewsets
from login.serializers import UserLoginSerializer, UserSerializer
from rest_framework.response import Response
from .statuscode import ResponseCode, APIResponse

# Create your views here.

class HRLoginView(APIView):
    serializer_class = UserLoginSerializer
    def post(self,request):
        serializer = self.serializer_class(data=request.data)
        valid_serializer = serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        if valid_serializer:
            message = 'Created!'
            response_code = ResponseCode.CREATED
        # else:
        #     message = 'Bad Request'
        #     response_code = ResponseCode.BAD_REQUEST
        response = APIResponse(data=data,message=message, code=response_code)
        return response.build_response()
        # return Response(serializer.validated_data, status=status.HTTP_200_OK)



class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def perform_create(self, serializer):
        validated_data = serializer.validated_data
        validated_data['is_staff'] = True
        validated_data['is_superuser'] = True
        return super().perform_create(serializer)
        








