# Generated by Django 3.2.19 on 2023-06-08 06:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('login', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='role',
            field=models.CharField(choices=[('AD', 'ADMIN'), ('HR', 'HR'), ('MEM', 'MEMBER')], max_length=20),
        ),
    ]
