from rest_framework import serializers
from .models import User
from django.contrib.auth.hashers import make_password
from __helpers.utils import decrypt_password, encrypt_password
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth import authenticate
from .statuscode import ResponseCode, APIResponse



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = ('groups','is_staff','last_login','is_superuser',)

    def create(self,validated_data):
        password = validated_data.pop('password')
        hashed_password = make_password(password)
        validated_data['password'] = hashed_password
        user = super().create(validated_data)
        return user



class UserLoginSerializer(serializers.Serializer):
    '''
        Serializer for Ackroo Login that will yield refresh token
        and access token using django-rest-simple-jwt 
    '''
    email = serializers.CharField()
    password = serializers.CharField()

    def validate(self,validated_data):
        try:
            email = validated_data.get('email')
            password = validated_data.get('password')
            user = authenticate(email=email,password=password)
            if not user:
                 raise serializers.ValidationError('Invalid email or password')
            refresh = RefreshToken.for_user(user)
            return {
                'access': str(refresh.access_token),
                'refresh': str(refresh)
            }
        except Exception as e:
            raise serializers.ValidationError(f'{str(e)}')
