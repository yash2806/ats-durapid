from enum import Enum
from rest_framework import status
from rest_framework.response import Response


class ResponseCode:
    SUCCESS = {'value': 'success', 'status': 200}
    CREATED = {'value': 'created', 'status': 201}
    NO_CONTENT = {'value': 'no content', 'status': 204}
    UNAUTHORIZED = {'value': 'unauthorized', 'status': 401}
    FORBIDDEN = {'value': 'forbidden', 'status': 403}
    NOT_FOUND = {'value': 'not found', 'status': 404}
    SERVER_ERROR = {'value': 'server error', 'status': 500}
    FAILURE = {'value': 'failure', 'status': 400}


class APIResponse:
    def __init__(self, data=None, message=None, code=ResponseCode.SUCCESS):
        self.data = data
        self.message = message
        self.code = code

    def build_response(self):
        response_data = {
            'code': self.code['status'],
            'message': self.message,
            'data': self.data,
        }
        return Response(response_data, status=self.code['status'])



