from django.db import models
from enum import Enum
from .managers import UserAccountManager
from django.contrib.auth.models import AbstractUser ,Group, Permission


class Roles(str,Enum):
    ADMIN  = "AD"
    HR = "HR"
    MEMBER = "MEM"

RolesChoices = (
    (Roles.ADMIN.value,Roles.ADMIN.name),
    (Roles.HR.value,Roles.HR.name),
    (Roles.MEMBER.value,Roles.MEMBER.name),
)

class User(AbstractUser):
    username = None
    role = models.CharField(max_length=20,choices=RolesChoices)
    email = models.EmailField(unique=True,max_length=255)
    groups = models.ManyToManyField(Group, related_name='userlogins', blank=True)
    objects = UserAccountManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    @property
    def get_role_value(self):
        pass


        

    

